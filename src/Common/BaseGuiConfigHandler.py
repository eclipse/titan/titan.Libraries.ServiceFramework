#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
import json
from Common.EditableGroupRights import EditableGroupRights
from Common.EditableGroupRights import HELP as editableGroupsHelp

DSHELP = [
    {
        "dataElement": {
            "description": "Help on the available elements.",
            "name": "help",
            "valueType": "octetstringType"
        }
    }
]

LOGIN_CONFIG = '''{
    "availableApps": [
        {
            "directory": "WebApplications/Authentication",
            "name": "Login"
        }
    ],
    "defaultApp": "Login"
}'''

class BaseGuiConfigHandler:

    SOURCE_ID = 'WebApplications'

    def __init__(self, directory):
        self._groupRightsHandler = EditableGroupRights(directory, [])

    def createConfig(self, userGroups):
        apps = []

        apps.append({
            'directory': 'WebApplications/Authentication',
            'name': 'Logout',
            'icon': 'WebApplications/Authentication/logout.png',
            'params': {
                'logout': True
            }
        })

        return {'availableApps': apps, 'defaultApp': 'Logout'}

    def hasAccessRight(self, webApp, userGroups):
        for group in userGroups:
            if webApp in self._groupRightsHandler.getGroupRights(group):
                return True
        return False

    def _getDataHandler(self, request, userCredentials):
        element = request['element']
        params = request['params']

        if element == 'help':
            dataElements = DSHELP + editableGroupsHelp
            help = {'sources': [{'source': self.SOURCE_ID, 'dataElements': dataElements}]}
            return {'node': {'val': json.dumps(help).encode('hex'), 'tp': 5}}

        return self._groupRightsHandler.handleGetData(element, params, userCredentials)

    def _setDataHandler(self, request, userCredentials):
        element = request['element']
        params = request['params']
        content = request['content']

        return self._groupRightsHandler.handleSetData(element, params, content, userCredentials)

    def handleMessage(self, method, path, headers, body, userCredentials, response):
        if userCredentials['username'] is None:
            response['body'] = LOGIN_CONFIG
        else:
            response['body'] = json.dumps(self.createConfig(userCredentials['groups']))
        response['headers']['Content-Type'] = 'application/json'

    def getDataSourceHandlers(self):
        return {
            self.SOURCE_ID: {
                'getDataHandler': self._getDataHandler,
                'setDataHandler': self._setDataHandler
            }
        }

    def close(self):
        pass