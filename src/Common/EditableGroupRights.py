#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
import os, json
try:
    from jsonschema import validate
except:
    def validate(*args):
        return True

HELP = [
    {
        "dataElement": {
            "description": "The list of groups that have rights.",
            "name": "HandledGroups",
            "valueType": "charstringlistType",
            "typeDescriptor": {
                "isListOf": True,
                "typeName": "Group"
            }
        }
    },
    {
        "dataElement": {
            "description": "The json schema of the group rights as a string encoded json.",
            "name": "GroupRightsSchema",
            "valueType": "charstringType"
        }
    },
    {
        "dataElement": {
            "description": "The rights of the group as a string encoded json.",
            "name": "GroupRights",
            "valueType": "intType",
            "parameters": [
                {
                    "description": "The group name.",
                    "exampleValue": "some_group",
                    "name": "Group",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Group"
                        }
                    }
                }
            ]
        }
    }
]

class EditableGroupRights:

    def __init__(self, directory, default):
        self._directory = directory
        self._default = default
        with open(os.path.join(directory, 'groupRights.json'), 'r') as f:
            self._groupRights = json.load(f)
        with open(os.path.join(self._directory, 'groupRightsSchema.json'), 'r') as f:
            self._schema = json.load(f)

    def _saveGroupRights(self):
        with open(os.path.join(self._directory, 'groupRights.json'), 'w') as f:
            json.dump(self._groupRights, f, indent = 4)

    def handleGetData(self, element, params, userCredentials):
        if element == 'GroupRightsSchema':
            return {'node': {'val': json.dumps(self._schema), 'tp': 4}}
        elif element == 'GroupRights' and len(params) == 1 and params[0]['paramName'] == 'Group':
            return {'node': {'val': json.dumps(self._groupRights.get(params[0]['paramValue'], self._default)), 'tp': 4}}
        elif element == 'HandledGroups':
            return {'list': [{'node': {'val': group, 'tp': 10}} for group in self._groupRights]}

    def handleSetData(self, element, params, content, userCredentials):
        if 'admin' in userCredentials['groups']:
            if element == 'GroupRights' and len(params) == 1 and params[0]['paramName'] == 'Group':
                rights = json.loads(content)
                if rights is None:
                    self._groupRights.pop(params[0]['paramValue'], self._default)
                    self._saveGroupRights()
                    return {'node': {'val': '0', 'tp': 1}}
                elif validate(rights, self._schema):
                    self._groupRights[params[0]['paramValue']] = rights
                    self._saveGroupRights()
                    return {'node': {'val': '0', 'tp': 1}}
                else:
                    return {'node': {'val': 'Rigths does not match the schema.', 'tp': 4}}

    def getGroupRights(self, group):
        return self._groupRights.get(group, self._default)
