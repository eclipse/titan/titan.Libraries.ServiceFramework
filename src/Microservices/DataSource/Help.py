#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
DSHELP = [
    {
        "dataElement": {
            "description": "Help on all available elements.",
            "name": "help",
            "valueType": "octetstringType"
        }
    },
    {
        "dataElement": {
            "description": "The available sources.",
            "name": "Sources",
            "valueType": "charstringlistType"
        }
    },
    {
        "dataElement": {
            "description": "Whether a dataelement returned by the request specified in the parameters is handled by a handler.",
            "name": "dataElementPresent",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The source of the request.",
                    "exampleValue": "DataSource",
                    "name": "Source",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                },
                {
                    "description": "The element of the request.",
                    "exampleValue": "Sources",
                    "name": "Element",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                },
                {
                    "description": "A parameter name hat must be followed by a ParamValue.",
                    "exampleValue": "Param1",
                    "name": "ParamName",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                },
                {
                    "description": "The parameter value.",
                    "exampleValue": "Value_of_Param1",
                    "name": "ParamValue",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "The size of a list or 1 for single elements returned by the request specified in the parameters.",
            "name": "sizeOf",
            "valueType": "intType",
            "parameters": [
                {
                    "description": "The source of the request.",
                    "exampleValue": "DataSource",
                    "name": "Source",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                },
                {
                    "description": "The element of the request.",
                    "exampleValue": "Sources",
                    "name": "Element",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                },
                {
                    "description": "A parameter name hat must be followed by a ParamValue.",
                    "exampleValue": "Param1",
                    "name": "ParamName",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                },
                {
                    "description": "The parameter value.",
                    "exampleValue": "Value_of_Param1",
                    "name": "ParamValue",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Check whether at least one of the elements of the parameter is true.",
            "name": "exists",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "[\"true\", \"false\"]",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "charstringlistType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Check whether all the elements of the parameter are true.",
            "name": "forAll",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "[\"true\", \"false\"]",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "charstringlistType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Return the sum of the elements of the parameter.",
            "name": "sum",
            "valueType": "floatType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "[\"2.5\", \"1.3\"]",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "charstringlistType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Check whether the two parameters are equal.",
            "name": "==",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "some string",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                },
                {
                    "description": "The second parameter.",
                    "exampleValue": "some other string",
                    "name": "Par2",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Check whether the two parameters are not equal.",
            "name": "!=",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "some string",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                },
                {
                    "description": "The second parameter.",
                    "exampleValue": "some other string",
                    "name": "Par2",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Check whether the first parameter is greater than the second.",
            "name": ">",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "123",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "floatType"
                    }
                },
                {
                    "description": "The second parameter.",
                    "exampleValue": "234.5",
                    "name": "Par2",
                    "typeDescriptor": {
                        "valueType": "floatType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Check whether the first parameter is equal to or greater than the second.",
            "name": ">=",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "123",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "floatType"
                    }
                },
                {
                    "description": "The second parameter.",
                    "exampleValue": "234.5",
                    "name": "Par2",
                    "typeDescriptor": {
                        "valueType": "floatType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Check whether the first parameter is lesser than the second.",
            "name": "<",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "123",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "floatType"
                    }
                },
                {
                    "description": "The second parameter.",
                    "exampleValue": "234.5",
                    "name": "Par2",
                    "typeDescriptor": {
                        "valueType": "floatType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Check whether the first parameter is equal to or lesser than the second.",
            "name": "<=",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "123",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "floatType"
                    }
                },
                {
                    "description": "The second parameter.",
                    "exampleValue": "234.5",
                    "name": "Par2",
                    "typeDescriptor": {
                        "valueType": "floatType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Returns the logical and of the parameters (any number of parameters are supported).",
            "name": "and",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "true",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "boolType"
                    }
                },
                {
                    "description": "The second parameter.",
                    "exampleValue": "false",
                    "name": "Par2",
                    "typeDescriptor": {
                        "valueType": "boolType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Returns the logical or of the parameters (any number of parameters are supported).",
            "name": "or",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "true",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "boolType"
                    }
                },
                {
                    "description": "The second parameter.",
                    "exampleValue": "false",
                    "name": "Par2",
                    "typeDescriptor": {
                        "valueType": "boolType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Returns the logical inverse of the parameter.",
            "name": "not",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The first parameter.",
                    "exampleValue": "true",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "boolType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Checks whther the pattern in the second parameter matches the string in the first parameter.",
            "name": "match",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The string to search.",
                    "exampleValue": "some string",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                },
                {
                    "description": "The pattern to match.",
                    "exampleValue": "??m*str*",
                    "name": "Par2",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Checks whther the pattern in the second parameter matches the string in the first parameter.",
            "name": "not match",
            "valueType": "boolType",
            "parameters": [
                {
                    "description": "The string to search.",
                    "exampleValue": "some string",
                    "name": "Par1",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                },
                {
                    "description": "The pattern to match.",
                    "exampleValue": "??m*str*",
                    "name": "Par2",
                    "typeDescriptor": {
                        "valueType": "charstringType"
                    }
                }
            ]
        }
    }
]