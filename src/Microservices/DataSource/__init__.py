#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
'''
DataSource is a handler that can be used as a hub for other handlers that provide a DsRestAPI interface.
It makes it possible to handle DsRestAPI requests that need no be handled be multiple microservices.
'''

from DataSource import DataSource

EXTENSION = 'api.appagent'

def createHandler(directory, *args):
    return DataSource(directory)