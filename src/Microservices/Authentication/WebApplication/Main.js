// Copyright (c) 2000-2023 Ericsson Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
var WebApplications = WebApplications || [];

WebApplications.push({'application': new Authentication_Application()});

function Authentication_Application() {
    "use strict";

    var v_appBase = new WebAppBase();
    var v_fileHandler;

    this.info = function() {
        return {
            defaultIcon: "WebApplications/Authentication/login.png",
            defaultName: "Login"
        };
    };

    this.load = function(p_webAppModel, p_params, p_framework)  {
        v_fileHandler = p_webAppModel.getFileHandler();
        if (p_params.logout) {
            v_appBase.load([], [], startLogoutApp, v_fileHandler);
        } else {
            v_appBase.load([], [], startLoginApp, v_fileHandler);
        }
    };

    this.unload = function(webappUnloaded) {
        v_appBase.unload(destroy);
        webappUnloaded(true);
    };
    
    function onWindowResize(event) {
        if (event.target == window) {
            $("#Login").height(ViewUtils.getSuggestedHeight("Login"));
        }
    }

    function destroy() {
        $(window).off("resize", onWindowResize);
        $("#Login").remove();
    }
    
    function loadHtml(callback) {
        v_fileHandler.loadFile("WebApplications/Authentication/Main.html", function(ok, html) {
            $("#TSGuiFrameworkMain").append(html);
            $("#Login_WebAppStyle").load("WebApplications/Authentication/Main.css", function() {
                $(window).on("resize", onWindowResize);
                $("#Login").height(ViewUtils.getSuggestedHeight("Login"));
                $('form').submit(function(event) {
                    login($('input[name="username"]').val(), $('input[name="password"]').val());
                    event.preventDefault();
                });
                $('input[name="username"]').focus()
                callback(true);
            });
        });
    }

    function login(username, password) {
        var credentials = {
            'username': username,
            'password': password
        };
        
        $.ajax({
            url: 'login/api.authenticate',
            type: 'POST',
            data: JSON.stringify(credentials),
            cache: false,
            success: function() {
                localStorage.setItem('username', credentials.username);
                localStorage.setItem('password', credentials.password);
                location.reload(true);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#Login_Error').text(jqXHR.responseText);
            }
        });
    }
    
    function startLoginApp(callback) {
        loadHtml(callback);
    }
    
    function startLogoutApp(callback) {
        $.ajax({
            url: 'logout/api.authenticate',
            type: 'POST',
            data: '',
            cache: false,
            success: function() {
                localStorage.removeItem("username");
                localStorage.removeItem("password");
                location.reload(true);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                location.reload(true);
            }
        });
        callback(true);
    }
}

//# sourceURL=Authentication\Main.js