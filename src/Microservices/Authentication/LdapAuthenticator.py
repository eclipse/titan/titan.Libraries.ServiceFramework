#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
'''
This module provides an authenticate(username, password) function that uses LDAP to authenticate a user.
'''

import ldap, logging

#LDAP_ADDRESS = 'ldaps://159.107.49.152:3269'
#LDAP_ADDRESS = 'ldaps://ldap-egad.internal.ericsson.com:3269'
#LDAP_ADDRESS = 'ldaps://ericsson.se:636')
LDAP_ADDRESS = 'ldaps://sesbiwegad0001.ericsson.se:636'
USER_PREFIX = 'ERICSSON\\'

def _setLdapOptions(ld):
    ld.set_option(ldap.OPT_NETWORK_TIMEOUT, 5) # timeout on ldap connection
    ld.set_option(ldap.OPT_TIMEOUT, 5) # timeout on ldap actions
    ld.set_option(ldap.OPT_REFERRALS, 0)
    ld.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
    ld.set_option(ldap.OPT_DEBUG_LEVEL, 255)

def authenticate(username, password):
    # admin allowed by default for now
    if username == 'admin' and password == 'admin':
        return True
    logging.getLogger(__name__).info('LDAP authentication for ' + username)
    if password.strip() == '':
        raise Exception('Password must not be empty')
    if username.strip() == '':
        raise Exception('Username must not be empty')
    username = USER_PREFIX + username

    # Must be set before initialize. The rest of the option can be set afterwards
    ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)

    try:
        ld = ldap.initialize(LDAP_ADDRESS)
        _setLdapOptions(ld)
        ld.simple_bind_s(username, password)
    except Exception as e:
        # Extract textual error message from the exception
        errorMessage = e.args[0]['desc']
        raise Exception(errorMessage)
    finally:
        # Must be invoked otherwise upcoming requests may be denied by LDAP server
        ld.unbind_s()

    return True