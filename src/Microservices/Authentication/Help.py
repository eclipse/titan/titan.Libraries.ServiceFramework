#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
DSHELP = [
    {
        "dataElement": {
            "description": "Help on the available elements.",
            "name": "help",
            "valueType": "octetstringType"
        }
    },
    {
        "dataElement": {
            "description": "The available groups.",
            "name": "ListGroups",
            "valueType": "charstringlistType",
            "typeDescriptor": {
                "isListOf": True,
                "typeName": "Group"
            }
        }
    },
    {
        "dataElement": {
            "description": "Add a user group.",
            "name": "AddGroup",
            "valueType": "charstringType"
        }
    },
    {
        "dataElement": {
            "description": "Remove a user group.",
            "name": "RemoveGroup",
            "valueType": "intType",
            "parameters": [
                {
                    "description": "The group name.",
                    "exampleValue": "some_group",
                    "name": "Group",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Group"
                        }
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Add user to the group.",
            "name": "AddUserToGroup",
            "valueType": "charstringType",
            "parameters": [
                {
                    "description": "The group name.",
                    "exampleValue": "some_group",
                    "name": "Group",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Group"
                        }
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "List the users in the group.",
            "name": "ListUsersInGroup",
            "valueType": "charstringlistType",
            "parameters": [
                {
                    "description": "The group name.",
                    "exampleValue": "some_group",
                    "name": "Group",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Group"
                        }
                    }
                }
            ],
            "typeDescriptor": {
                "isListOf": True,
                "typeName": "User"
            }
        }
    },
    {
        "dataElement": {
            "description": "List the groups of a user.",
            "name": "ListGroupsOfUser",
            "valueType": "charstringlistType",
            "parameters": [
                {
                    "description": "The user signum.",
                    "exampleValue": "eabcxyz",
                    "name": "User",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "User"
                        }
                    }
                }
            ],
            "typeDescriptor": {
                "isListOf": True,
                "typeName": "Group"
            }
        }
    },
    {
        "dataElement": {
            "description": "Remove the user from the group.",
            "name": "RemoveUserFromGroup",
            "valueType": "intType",
            "parameters": [
                {
                    "description": "The user signum.",
                    "exampleValue": "eabcxyz",
                    "name": "User",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "User"
                        }
                    }
                },
                {
                    "description": "The group name.",
                    "exampleValue": "some_group",
                    "name": "Group",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Group"
                        }
                    }
                }
            ]
        }
    }
]