#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
'''
This package contains the microservices.
They are dynamically loaded by AppAgent before starting the http server.

Microservices must implement the following interface (in their __init__.py):
    EXTENSION
        string (e.g '.control')
    createHandler(directory)
        function, that must return a http message handler that will be associated with the EXTENSION
        the directory parameter is a path to the microservice's directory so the handler can load resources

The message handlers must implement the following interface functions:
    handleMessage(method, path, headers, body, userCredentials, response):
        method: either GET or POST
        path: the uri of the request
        headers: the request headers
        body: the request body
        userCredentials: the user credentials dictionary with the following members:
            username: the username
            password: the password of the user
            groups: the set of groups that the user belongs to
        response: the response dictionary, the following members can be set:
            returnCode: the return code (default: 200)
            mimeType: the mime type (default: 'text/plain')
            body: the response body (default: empty string)
    close():
        called when the server is stopped so the handler can do some cleanup

Additionally, the handler can implement the following function:
    getDataSourceHandlers():
        this function must return a dictionary, for example:
        {
            'someSourceId': {
                'getDataHandler': a_function_that_can_handle_getData_requests(request, userCredentials)
                'setDataHandler': a_function_that_can_handle_setData_requests(request, userCredentials)
            },
            'otherSourceId': {...}
        }

        When the function exists it will be used to add the handlers to the DataSource.
        Depending on the source of a given request, the DataSource will call the appropriate handler with the getData or setData request dictionary.

If an microservice contains a GUI directory, AppAgent will create a symlink to that directory in the CustomizableContent, so it can appear on the GreenGUI.
'''