#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
'''
This application can be used to handle Playlists that can be used to issue specific requests by timers and conditions.
'''

from Playlist import Playlist

EXTENSION = 'api.playlist'

def createHandler(directory, *args):
    return Playlist(directory)