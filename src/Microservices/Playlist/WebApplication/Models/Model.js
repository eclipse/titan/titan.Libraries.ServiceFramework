// Copyright (c) 2000-2023 Ericsson Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function PlaylistEditor_Model(p_webAppModel) {
    "use strict";

    var v_baseModel = p_webAppModel;
    var v_dsRestAPI = new DsRestAPI("appagent");

    var v_currentName;
    var v_playlist = {};

    var v_this = this;

    ///////////////////// PLAYLIST HANDLING /////////////////////

    this.newPlaylist = function() {
        v_playlist = {};
        v_currentName = undefined
        return v_playlist;
    };

    this.deletePlaylist = function(name, callback) {
        function playlistDeleted(response) {
            if (response != undefined && response.node != undefined && response.node.tp == 1) {
                callback(true);
            } else {
                callback(false);
            }
        }

        v_dsRestAPI.setData(playlistDeleted, "Playlist", "Delete", "0", 1, [{
            "paramName": "Playlist",
            "paramValue": name
        }]);

        if (name == v_currentName) {
            v_currentName = undefined;
        }
    };

    this.loadPlaylist = function(name, callback) {
        function playlistLoaded(response) {
            if (response != undefined && response.node != undefined && response.node.tp == 4) {
                try {
                    convertToVisualizablePlaylist(JSON.parse(response.node.val));
                    v_currentName = name;
                    callback(true);
                } catch(e) {
                    callback(false);
                }
            } else {
                callback(false);
            }
        }

        v_dsRestAPI.getData(playlistLoaded, "Playlist", "Descriptor", [{
            "paramName": "Playlist",
            "paramValue": name
        }]);
    };

    this.savePlaylist = function(callback) {
        function playlistSaved(response) {
            if (response != undefined && response.node != undefined && response.node.tp == 4) {
                callback(true);
            } else {
                callback(false);
            }
        }

        v_dsRestAPI.setData(playlistSaved, "Playlist", "Descriptor", JSON.stringify(convertToUsablePlaylist(), null, 4), 4, [{
            "paramName": "Playlist",
            "paramValue": v_currentName
        }]);
    };

    this.savePlaylistAs = function(name, callback) {
        var oldName = v_currentName;
        v_currentName = name;

        function playlistSaved(ok) {
            if (!ok) {
                v_currentName = oldName;
            }
            callback(ok);
        }

        v_this.savePlaylist(playlistSaved);
    };

    this.currentlyEdited = function() {
        return v_currentName;
    };

    this.listPlaylists = function(callback) {
        function playlistsListed(response) {
            var list = [];
            if (response != undefined && response.list != undefined) {
                for (var i = 0; i < response.list.length; ++i) {
                    list.push(response.list[i].node.val);
                }
                callback(true, list);
            } else {
                callback(false);
            }
        }

        v_dsRestAPI.getData(playlistsListed, "Playlist", "Playlists");
    };

    this.playlistExists = function(name, callback) {
        function playlistsListed(ok, list) {
            callback(ok && list.indexOf(name) != -1);
        }

        v_this.listPlaylists(playlistsListed);
    };

    this.listGroups = function(callback) {
        function groupsListed(response) {
            var list = [];
            if (response != undefined && response.list != undefined) {
                for (var i = 0; i < response.list.length; ++i) {
                    list.push(response.list[i].node.val);
                }
                callback(true, list);
            } else {
                callback(false);
            }
        }

        v_dsRestAPI.getData(groupsListed, "Authentication", "ListGroupsOfUser", [{
            "paramName": "User",
            "paramValue": localStorage.username
        }]);
    };

    function convertToVisualizablePlaylist(playlist) {
        v_playlist = {};
        for (var i = 0; i < playlist.length; ++i) {
            var api = playlist[i].apiUrl;
            for (var j = 0; j < playlist[i].playlist.length; ++j) {
                var nextPlaylist = playlist[i].playlist[j];
                nextPlaylist.api = api;
                var filter = undefined;
                if (nextPlaylist.condition != undefined) {
                    filter = nextPlaylist.condition.expression;
                } else {
                    nextPlaylist.condition = {}
                }
                nextPlaylist.condition.expression = [{
                    "getData": {
                        "source": "DataSource",
                        "element": "Sources",
                        "filter": filter
                    }
                }]
                if (nextPlaylist.desktopData == undefined) {
                    nextPlaylist.desktopData = {
                        "top": i * 50,
                        "left": j * 50,
                        "visible": true
                    }
                }
                v_playlist[nextPlaylist.id] = nextPlaylist;
            }
        }
    }

    function convertToUsablePlaylist() {
        var playlist = [];
        var apis = {};
        var insertedIds = {};
        fillPlaylistByApi(playlist, apis);
        var notAllInserted = true;
        var inserted = true;
        if (Object.keys(v_playlist).length > 0) {
            while (notAllInserted && inserted) {
                notAllInserted = false;
                inserted = false;
                for (var id in v_playlist) {
                    if (v_playlist[id].api != undefined && !insertedIds[id]) {
                        if (numberOfUnfulfilledReferences(v_playlist[id], insertedIds) == 0) {
                            var toInsert = mcopy(v_playlist[id]);
                            toInsert.api = undefined;
                            toInsert.condition.expression = toInsert.condition.expression[0].getData.filter;
                            playlist[apis[v_playlist[id].api]].playlist.push(toInsert);
                            insertedIds[id] = true;
                            inserted = true;
                        } else {
                            notAllInserted = true
                        }
                    }
                }
            }
        }

        if (!inserted) {
            alert("Circular dependency found, using only the correct part.");
        }

        return playlist;
    }

    function fillPlaylistByApi(playlist, apis) {
        var missingApi = false;
        for (var id in v_playlist) {
            var api = v_playlist[id].api;
            if (api != undefined) {
                if (apis[api] == undefined) {
                    apis[api] = playlist.length;
                    playlist.push({
                        "apiUrl": api,
                        "playlist": []
                    });
                }
            } else {
                missingApi = true;
            }
        }

        if (missingApi) {
            alert("Some elements are missing an api, they will not be used.")
        }
    }

    function numberOfUnfulfilledReferences(element, insertedIds) {
        var count = 0;
        if (element.relativeTo != undefined) {
            for (var i = 0; i < element.relativeTo.length; ++i) {
                if (!insertedIds[element.relativeTo[i]]) {
                    ++count;
                }
            }
        }
        return count
    }

    ///////////////////// EDITOR MODEL HANDLING /////////////////////

    function getUnusedId() {
        var i = 0;
        while (v_playlist["" + i] != undefined) {
            i += 1
        }
        return "" + i
    }

    this.getEditorModels = function() {
        return v_playlist;
    };

    this.createEditorModel = function() {
        var id = getUnusedId();
        var playlist = {
            "id": id,
            "relativeTo": [],
            "startTime": 0,
            "api": window.location.protocol + "//" + window.location.host + "/api.appagent",
            "requests": [],
            "condition": {
                "expression": [{
                    "getData": {
                        "source": "DataSource",
                        "element": "Sources"
                    }
                }]
            },
            "desktopData": {
                "top": 0,
                "left": 300,
                "visible": true,
                "openNodes": []
            }
        };

        v_playlist[id] = playlist;

        return {
            "id": id,
            "model": playlist
        };
    };

    this.deleteEditorModel = function(id) {
        delete v_playlist[id];
        for (var playlistId in v_playlist) {
            var index = v_playlist[playlistId].relativeTo.indexOf(id);
            if (index != -1) {
                v_playlist[playlistId].relativeTo.splice(index, 1);
            }
        }
    };

    this.playlistConnection = function(p_from, p_to) {
        var index = v_playlist[p_to].relativeTo.indexOf(p_from);
        if (index == -1) {
            v_playlist[p_to].relativeTo.push(p_from);
        } else {
            v_playlist[p_to].relativeTo.splice(index, 1);
        }
    };

    ///////////////////// CONFIG HANDLING /////////////////////

    this.getAppConfig = v_baseModel.getAppConfig;

    ///////////////////// USEFUL FUNCTIONS FOR VIEWMODELS /////////////////////

    function compareOptions(option1, option2) {
        if (option1.text < option2.text) {
            return -1;
        } else if (option1.text > option2.text) {
            return 1;
        } else {
            return 0;
        }
    }

    this.sortOptions = function(options) {
        options.sort(compareOptions);
    };

    this.getDsRestAPI = function() {
        return v_dsRestAPI;
    };

    this.getFileHandler = v_baseModel.getFileHandler;
}