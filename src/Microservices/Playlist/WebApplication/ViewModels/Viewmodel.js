// Copyright (c) 2000-2023 Ericsson Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function PlaylistEditor_ViewModel(p_model) {

    var v_model = p_model;
    var v_dsRestAPI = v_model.getDsRestAPI();
    var v_fileHandler = v_model.getFileHandler();
    var v_binder;

    var v_viewmodels = {};
    var v_help;

    this.elementEditorViewmodel = new PlaylistEditor_ElementEditor_ViewModel();
    this.filterElementEditorViewmodel = new PlaylistEditor_FilterElementEditor_ViewModel();

    this.init = function(p_callback) {
        function helpArrived(ok, help) {
            v_help = new HelpTreeBuilder(help).getHelpTree();
            p_callback(true);
        }

        v_model.getDsRestAPI().getHelp(helpArrived);
    };

    this.setBinder = function(binder) {
        v_binder = binder;
    };

    this.newPlaylist = v_model.newPlaylist;
    this.deletePlaylist = v_model.deletePlaylist;
    this.loadPlaylist = v_model.loadPlaylist;
    this.savePlaylist = v_model.savePlaylist;

    this.savePlaylistAs = function(group, name, callback) {
        v_model.savePlaylistAs(group + '/' + name, callback);
    };

    this.currentlyEdited = v_model.currentlyEdited;

    this.listPlaylists = function(callback) {
        v_model.listPlaylists(function(ok, list) {
            callback(createOptionsFromList(list))
        });
    }

    this.listGroups = function(callback) {
        v_model.listGroups(function(ok, list) {
            callback(createOptionsFromList(list))
        });
    }

    this.playlistExists = function(group, name, callback) {
        v_model.playlistExists(group + '/' + name, callback);
    };

    this.getEditorViewmodels = function() {
        v_viewmodels = {};
        var models = v_model.getEditorModels();
        for (var id in models) {
            v_viewmodels[id] = new PlaylistEditor_PlaylistItem_ViewModel(models[id], v_help);
        }
        return v_viewmodels;
    };

    this.createEditorViewmodel = function() {
        var obj = v_model.createEditorModel();
        v_viewmodels[obj.id] = new PlaylistEditor_PlaylistItem_ViewModel(obj.model, v_help);
        return {
            "id": obj.id,
            "viewmodel": v_viewmodels[obj.id]
        };
    };

    this.deleteEditorViewmodel = function(id) {
        v_model.deleteEditorModel(id);
        delete v_viewmodels[id];
    };

    this.playlistConnection = v_model.playlistConnection;

    this.getHelp = function() {
        return v_help;
    };

    this.loadFile = v_model.getFileHandler().loadFile;

    function createOptionsFromList(list) {
        if (list == undefined) {
            list = [];
        }

        var options = [];
        for (var i = 0; i < list.length; ++i) {
            options.push({
                "text": list[i],
                "value": list[i]
            });
        }

        return options;
    }
}