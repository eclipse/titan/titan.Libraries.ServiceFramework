// Copyright (c) 2000-2023 Ericsson Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function PlaylistEditor_FilterElementEditor_ViewModel() {
    "use strict";

    var FILTER_SCHEMA = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Edit filter",
        "type": "object",
        "oneOf": [
            {
                "title": "data value",
                "type": "object",
                "properties": {
                    "dataValue": {
                        "description": "A value",
                        "type": "string",
                        "default": "true"
                    }
                },
                "required": ["dataValue"],
                "additionalProperties": false
            },
            {
                "title": "request",
                "type": "object",
                "properties": {
                    "request": {
                        "description": "A request that will be converted to a value during the filter evaluation",
                        "type": "object",
                        "properties": {
                            "source": {
                                "description": "The source",
                                "type": "string"
                            },
                            "element": {
                                "description": "The element",
                                "type": "string"
                            },
                            "ptcname": {
                                "description": "The ptc name",
                                "type": "string"
                            }
                        },
                        "required": ["source", "element"],
                        "additionalProperties": false
                    }
                },
                "required": ["request"],
                "additionalProperties": false
            }
        ]
    };

    var v_filter;

    this.setFilter = function(p_filter) {
        v_filter = p_filter;
    };

    this.getJSONData = function(callback) {
        var filter = mcopy(v_filter);
        if (filter.request != undefined) {
            delete filter.request.params;
            delete filter.request.remapTo;
        }
        console.log(filter);
        callback(filter);
    };

    this.setJSONData = function(json) {
        if (json.dataValue != undefined) {
            v_filter.dataValue = json.dataValue;
            v_filter.request = undefined;
        } else if (json.request != undefined) {
            if (v_filter.request != undefined) {
                v_filter.request.source = json.request.source;
                v_filter.request.element = json.request.element;
                v_filter.request.ptcname = json.request.ptcname;
            } else {
                v_filter.request = json.request;
            }
            v_filter.dataValue = undefined;
        }
    };

    this.getSchema = function() {
        return FILTER_SCHEMA;
    };
}