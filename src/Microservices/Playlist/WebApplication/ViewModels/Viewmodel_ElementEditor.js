// Copyright (c) 2000-2023 Ericsson Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function PlaylistEditor_ElementEditor_ViewModel() {
    "use strict";

    var SETDATA_SCHEMA = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "title": "SetData request",
        "properties": {
            "source": {
                "type": "string"
            },
            "ptcname": {
                "type": "string"
            },
            "element": {
                "type": "string"
            },
            "content": {
                "type": "string"
            },
            "tp": {
                "type": "integer"
            },
            "indxsInList": {
                "type": "array",
                "items": {
                    "title": "index",
                    "type": "integer"
                },
                "format": "table"
            },
            "params": {
                "type": "array",
                "items": {
                    "title": "param",
                    "type" : "object",
                    "additionalProperties": false,
                    "properties": {
                        "paramName": {
                            "type": "string"
                        },
                        "paramValue": {
                            "type": "string"
                        }
                    },
                    "required": [
                        "paramName",
                        "paramValue"
                    ]
                },
                "format": "table"
            }
        },
        "required": [
            "source",
            "element",
            "content",
            "tp"
        ]
    };

    var GETDATA_SCHEMA = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "title": "GetData request",
        "properties": {
            "source": {
                "type": "string"
            },
            "ptcname": {
                "type": "string"
            },
            "element": {
                "type": "string"
            },
            "params": {
                "type": "array",
                "items": {
                    "title": "param",
                    "type" : "object",
                    "additionalProperties": false,
                    "properties": {
                        "paramName": {
                            "type": "string"
                        },
                        "paramValue": {
                            "type": "string"
                        }
                    },
                    "required": [
                        "paramName",
                        "paramValue"
                    ]
                },
                "format": "table"
            }
        },
        "required": [
            "source",
            "element"
        ]
    };

    var v_request;

    this.setRequest = function(p_request) {
        v_request = p_request;
    };

    this.getJSONData = function(callback) {
        var request;
        if (v_request.getData != undefined) {
            request = mcopy(v_request.getData);
        } else {
            request = mcopy(v_request.setData);
        }
        callback(request);
    };

    this.setJSONData = function(json) {
        if (v_request.getData != undefined) {
            v_request.getData.source = json.source;
            v_request.getData.element = json.element;
            v_request.getData.ptcname = json.ptcname;
            v_request.getData.params = json.params;
        } else {
            v_request.setData.source = json.source;
            v_request.setData.element = json.element;
            v_request.setData.ptcname = json.ptcname;
            v_request.setData.params = json.params;
            v_request.setData.content = json.content;
            v_request.setData.tp = json.tp;
            v_request.setData.indxsInList = json.indxsInList;
        }
    };

    this.getSchema = function() {
        if (v_request.getData != undefined) {
            return GETDATA_SCHEMA;
        } else {
            return SETDATA_SCHEMA;
        }
    };
}