// Copyright (c) 2000-2023 Ericsson Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
var WebApplications = WebApplications || [];

WebApplications.push({'application': new Playlist_Application()});

function Playlist_Application() {
    "use strict";
    
    var v_params;
    var v_appBase = new WebAppBase();
    var v_webAppModel;

    var v_model;
    var v_viewmodel;
    var v_view;
    var v_binder;

    this.info = function() {
        return {
            defaultIcon: "WebApplications/GuiEditor/Res/main_icon.png",
            defaultName: "Playlist Editor"
        };
    };

    this.load = function(p_webAppModel, p_params, p_framework)  {
        v_params = p_params;
        v_webAppModel = p_webAppModel;

        new MultipleDirectoryListTask(
            [
                "WebApplications/Playlist/Models",
                "WebApplications/Playlist/Views",
                "WebApplications/Playlist/ViewModels"
            ],
            v_webAppModel.getFileHandler()
        ).taskOperation(function(ok, resources) {
            var jsfiles = resources.jsfiles;
            jsfiles.push("Utils/DsRestAPI/DsRestAPI.js");
            jsfiles.push("Utils/DsRestAPI/DsRestAPIComm.js");
            jsfiles.push("WebApplications/GuiEditor/Views/View_Connections.js");
            v_appBase.load(jsfiles, [], start, v_webAppModel.getFileHandler());
        });
    };

    this.unload = function(webappUnloaded) {
        v_appBase.unload(destroy);
        webappUnloaded(true);
    };

    function destroy() {
        v_view.destroy();

        v_model = undefined;
        v_view = undefined;
        v_viewmodel = undefined;
        v_binder = undefined;
    }

    function start(p_callback) {
        v_model = new PlaylistEditor_Model(v_webAppModel);
        v_viewmodel = new PlaylistEditor_ViewModel(v_model);
        v_view = new PlaylistEditor_View(v_viewmodel, "TSGuiFrameworkMain", "PlaylistEditor_MainView");
        v_binder = new PlaylistEditor_Binder(v_viewmodel, v_view);
        v_viewmodel.setBinder(v_binder);

        function callback(ok, data) {
            if (ok) {
                v_view.applicationCreated();
                v_binder.fullRefresh();
            } else {
                alert(data);
            }
            if (p_callback != undefined) {
                p_callback();
            }
        }

        function playlistLoaded() {
            new SyncTaskList([new GenericTask(v_viewmodel.init), new GenericTask(v_view.init)], callback).taskOperation();
        }

        if (v_params.playlist != undefined) {
            v_model.loadPlaylist(v_params.playlist, playlistLoaded);
        } else {
            v_model.newPlaylist();
            playlistLoaded();
        }
    }
}

function PlaylistEditor_Binder(p_viewModel, p_view) {
    "use strict";
    var v_viewmodel = p_viewModel;
    var v_view = p_view;

    this.fullRefresh = function() {
        v_view.fullRefresh();
    };
}
//# sourceURL=PlaylistEditor\Main.js