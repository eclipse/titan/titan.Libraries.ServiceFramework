// Copyright (c) 2000-2023 Ericsson Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function PlaylistEditor_PlaylistItem_View(p_id, p_viewmodel, p_parent, p_helpTree) {

    var v_playlistId = p_id;
    var v_id = 'Playlist_Editor_' + v_playlistId;
    var v_viewmodel = p_viewmodel;
    var v_parent = p_parent;
    var v_desktopData = v_viewmodel.getDesktopData();
    var v_helpTree = p_helpTree;
    var v_tree;

    var v_jsTreeUtils = new JsTreeUtils();
    var v_zIndex = 800;

    var v_this = this;

    this.applicationCreated = function() {
        createEditorDiv();
        v_tree = $('#' + v_id + '_Tree');
        setupCallbacks();

        createTree();

        $("#" + v_id).on('click', function(){
            v_parent.setFocusedObj(v_this);
        });
        $("#" + v_id).on('dragstart', function(){
            v_parent.setFocusedObj(v_this);
        });
    };

    this.destroy = function() {
        $("#" + v_id).remove();
    };

    this.setDefaultZidx = function() {
        $("#" + v_id).css("z-index", 800);
        v_zIndex = 800;
        v_parent.refreshConnections();
    };

    this.setZidx = function() {
        $("#" + v_id).css("z-index", 1000);
        v_zIndex = 1000;
        v_parent.refreshConnections();
    };

    this.deletePressed = function() {
        var selected = v_tree.jstree("get_selected")[0];
        if (selected.length != undefined) {
            var path = v_jsTreeUtils.getPath(v_tree.jstree("get_node", selected)).path;
            if ((path.length == 1 && (path[0] == 1 || path[0] == 2)) || (path.length == 2 && path[0] == 4 && path[1] < 3)) {
                v_viewmodel.changeValue(path);
            } else if (path.length == 1 && path[0] == 3) {
                v_viewmodel.deleteRequest();
            } else if (path.length > 1 && path[0] == 3) {
                path.shift();
                v_viewmodel.requestBuilder.deleteRequest(path);
            } else if (path.length == 2 && path[0] == 4 && path[1] == 3) {
                v_viewmodel.filterBuilder.deleteFilterPart([0], []);
            } else if (path.length > 2 && path[0] == 4) {
                path.shift();
                path.shift();
                path.shift();
                v_viewmodel.filterBuilder.deleteFilterPart([0], path);
            }

            setTimeout(createTree, 0);
        }
    };

    this.isNodeFromTree = function(id) {
        return v_tree.jstree("get_node", id);
    };

    function createEditorDiv() {
        var html =
        '<div id="' + v_id + '" class="PlaylistEditor_Editor">' +
            '<div id="' + v_id + '_Header" class="PlaylistEditor_EditorHeader">' +
                '<label id="' + v_id + '_Name" class="PlaylistEditor_EditorLabel"></label>' +
                '<button id="' + v_id + '_Minimize" class="PlaylistEditor_EditorButton">_</button>' +
                '<button id="' + v_id + '_Close" class="PlaylistEditor_EditorButton">X</button>' +
            '</div>' +
            '<div id="' + v_id + '_Tree"></div>' +
        '</div>'

        $('#PlaylistEditor_Playground').append(html);

        var parentOffset = $("#PlaylistEditor_Playground").offset();
        var offset = {
            "top": v_desktopData.top + parentOffset.top,
            "left": v_desktopData.left + parentOffset.left
        }

        $("#" + v_id).offset(offset);
        $("#" + v_id).offset(offset);

        $("#" + v_id).draggable({
            stop: function(event, ui) {
                redrawOnDrag(event, ui);
                $("#" + v_id).height("auto");
                $("#" + v_id).width("auto");
            },
            handle: "#" + v_id + "_Header",
            drag: redrawOnDrag,
            containment: [0, 0, 20000, 20000]
        });

        if (!v_desktopData.visible) {
            v_tree.slideToggle(0);
            changeMinimizeButtonText();
        }
    }

    function changeMinimizeButtonText() {
        if (!v_desktopData.visible) {
            document.getElementById(v_id + '_Minimize').innerHTML = "+";
        } else {
            document.getElementById(v_id + '_Minimize').innerHTML = "_";
        }
    }

    function redrawOnDrag(event, ui) {
        v_parent.refreshConnections(v_this);
        if (event.type == "dragstop") {
            var parentOffset = $("#PlaylistEditor_Playground").offset();
            var offset = $("#" + v_id).offset();
            v_desktopData.top = offset.top - parentOffset.top + $("#PlaylistEditor_Playground").scrollTop();
            v_desktopData.left = offset.left - parentOffset.left + $("#PlaylistEditor_Playground").scrollLeft();
        }
    }

    function setupCallbacks() {
        $("#" + v_id + "_Minimize").click(function() {
            v_desktopData.visible = !v_desktopData.visible;
            v_tree.slideToggle("fast", function() {
                v_parent.refreshConnections(v_this);
            });
            changeMinimizeButtonText();
            $("#" + v_id).width("auto");
        });

        $("#" + v_id + "_Close").click(function() {
            v_parent.deleteEditorView(v_playlistId);
        });
    }

    function refresh() {
        createTree();
        v_parent.refreshConnections(v_this);
    }

    function createTree() {
        var data = v_viewmodel.getRepresentation();
        v_tree.jstree("destroy");
        v_tree = v_tree.jstree({
            "core": {
                "data": data,
                "check_callback" : function(operation, node, node_parent, node_position, more) {
                    if (operation == "copy_node" && v_jsTreeUtils.isNodeFromTree(node, v_helpTree) && v_parent.ctrlToggle && v_jsTreeUtils.getDepth(node, v_helpTree) > 1) {
                        return true;
                    } else if (operation == "copy_node" && !v_jsTreeUtils.isNodeFromTree(node, v_tree) && node.text == "next step" && node_parent.text == "next step") {
                        return true;
                    } else if (operation == "copy_node" && v_jsTreeUtils.isNodeFromTree(node, v_helpTree)) {
                        return dragFromHelpValidate(node.id, node_parent.id);
                    } else if (operation == "copy_node") {
                        return false;
                    } else {
                        return true;
                    }
                },
                "multiple": false,
                "animation": false,
                "worker": false
            },
            "plugins" : ["contextmenu", "dnd"],
            "dnd": {
                "always_copy": true
            },
            "contextmenu": {
                "items": function(node) {
                    var path = v_jsTreeUtils.getPath(node).path;
                    if ((path.length == 1 && (path[0] == 1 || path[0] == 2)) || (path.length == 2 && path[0] == 4 && path[1] < 3)) {
                        return {
                            "Edit": {
                                "label": "Edit",
                                "action": function(data) {
                                    var value = prompt("Please enter the new value.");
                                    if (value != undefined) {
                                        v_viewmodel.changeValue(path, value);
                                        setTimeout(refresh, 0);
                                    }
                                }
                            },
                            "Delete": {
                                "label": "Delete",
                                "action": function(data) {
                                    v_viewmodel.changeValue(path);
                                    setTimeout(refresh, 0);
                                }
                            }
                        };
                    } else if (path.length == 1 && path[0] == 3) {
                        return {
                            "Add": {
                                "label": "Add empty request",
                                "action": function(data) {
                                    v_viewmodel.requestBuilder.createEmptyRequest(0);
                                    setTimeout(refresh, 0);
                                }
                            },
                            "Delete": {
                                "label": "Delete whole request",
                                "action": function(data) {
                                    v_viewmodel.deleteRequest();
                                    setTimeout(refresh, 0);
                                }
                            }
                        };
                    } else if (path.length > 1 && path[0] == 3 && (node.data.highlight == "Set" || node.data.highlight == "Warning")) {
                        return {
                            "Edit": {
                                "label": "Edit request",
                                "action": function(data) {
                                    path.shift();
                                    var offset = data.reference.offset();
                                    offset.left += data.reference.width();
                                    v_parent.openElementEditor(v_viewmodel.requestBuilder.getRequestFromPath(path), offset, refresh);
                                }
                            },
                            "Delete": {
                                "label": "Delete",
                                "action": function(data) {
                                    path.shift();
                                    v_viewmodel.requestBuilder.deleteRequest(path);
                                    setTimeout(refresh, 0);
                                },
                                "separator_after": true
                            },
                            "ConvertToGetData": {
                                "label": "Convert to getData",
                                "action": function(data) {
                                    path.shift();
                                    v_viewmodel.requestBuilder.convertToGetData(path);
                                    setTimeout(refresh, 0);
                                }
                            }
                        };
                    } else if (path.length > 1 && path[0] == 3) {
                        return {
                            "Edit": {
                                "label": "Edit request",
                                "action": function(data) {
                                    path.shift();
                                    var offset = data.reference.offset();
                                    offset.left += data.reference.width();
                                    v_parent.openElementEditor(v_viewmodel.requestBuilder.getRequestFromPath(path), offset, refresh);
                                }
                            },
                            "EditFilter": {
                                "label": "Edit filter",
                                "action": function(data) {
                                    path.shift();
                                    var offset = data.reference.offset();
                                    offset.left += data.reference.width();
                                    v_parent.openFilterEditor(v_viewmodel, path, v_tree, offset, refresh);
                                },
                                "separator_after": true
                            },
                            "Add": {
                                "label": "Add child request",
                                "action": function(data) {
                                    v_desktopData.openNodes.push(path);
                                    path.shift();
                                    v_viewmodel.requestBuilder.addEmptyChildRequest(path, 0);
                                    setTimeout(refresh, 0);
                                }
                            },
                            "Copy": {
                                "label": "Copy",
                                "action": function(data) {
                                    path.shift();
                                    v_viewmodel.requestBuilder.copyRequest(path);
                                    setTimeout(refresh, 0);
                                }
                            },
                            "Delete": {
                                "label": "Delete",
                                "action": function(data) {
                                    path.shift();
                                    v_viewmodel.requestBuilder.deleteRequest(path);
                                    setTimeout(refresh, 0);
                                },
                                "separator_after": true
                            },
                            "ConvertToSetData": {
                                "label": "Convert to setData",
                                "action": function(data) {
                                    path.shift();
                                    v_viewmodel.requestBuilder.convertToSetData(path);
                                    setTimeout(refresh, 0);
                                }
                            }
                        }
                    } else if (path.length == 2 && path[0] == 4 && path[1] == 3) {
                        return {
                            "Create": {
                                "label": "Create",
                                "action": function(data) {
                                    v_viewmodel.filterBuilder.addFilterPart([0], []);
                                    v_desktopData.openNodes.push(path);
                                    setTimeout(refresh, 0);
                                }
                            },
                            "Delete": {
                                "label": "Delete",
                                "action": function(data) {
                                    v_viewmodel.filterBuilder.deleteFilterPart([0], []);
                                    setTimeout(refresh, 0);
                                }
                            }
                        };
                    } else if (path.length > 2 && path[0] == 4) {
                        return {
                            "Edit": {
                                "label": "Edit filter",
                                "action": function(data) {
                                    path.shift();
                                    path.shift();
                                    path.shift();
                                    var offset = data.reference.offset();
                                    offset.left += data.reference.width();
                                    v_parent.openFilterElementEditor(v_viewmodel.filterBuilder.getFilterPart([0], path), offset, refresh);
                                },
                                "separator_after": true
                            },
                            "Add": {
                                "label": "Add param",
                                "action": function(data) {
                                    var node = v_tree.jstree("get_node", data.reference);
                                    var originalPath = mcopy(path);
                                    var position = v_tree.jstree("get_children_dom", node).length;
                                    path.push(position);
                                    path.shift();
                                    path.shift();
                                    path.shift();
                                    if (v_viewmodel.filterBuilder.isValidToAddParamToFilterRequest([0], path)) {
                                        var paramName = prompt("New param name: ");
                                        if (paramName != undefined) {
                                            v_viewmodel.filterBuilder.addFilterPart([0], path, paramName);
                                            v_desktopData.openNodes.push(originalPath);
                                            setTimeout(refresh, 0);
                                        }
                                    } else {
                                        alert('Cannot add param to a dataValue');
                                    }
                                }
                            },
                            "RemapTo": {
                                "label": "Add remapTo",
                                "action": function(data) {
                                    var node = v_tree.jstree("get_node", data.reference);
                                    var originalPath = mcopy(path);
                                    var position = v_tree.jstree("get_children_dom", node).length;
                                    path.push(position);
                                    path.shift();
                                    path.shift();
                                    path.shift();
                                    if (v_viewmodel.filterBuilder.isValidToAddParamToFilterRequest([0], path)) {
                                        var paramName = "remapTo";
                                        v_viewmodel.filterBuilder.addFilterPart([0], path, paramName);
                                        v_desktopData.openNodes.push(originalPath);
                                        setTimeout(refresh, 0);
                                    } else {
                                        alert('Cannot add remapTo to a dataValue');
                                    }
                                }
                            },
                            "Delete": {
                                "label": "Delete",
                                "action": function(data) {
                                    path.shift();
                                    path.shift();
                                    path.shift();
                                    v_viewmodel.filterBuilder.deleteFilterPart([0], path);
                                    setTimeout(refresh, 0);
                                },
                                "separator_after": true
                            },
                            "ChangeParamName": {
                                "label": "Change param name",
                                "action": function(data) {
                                    path.shift();
                                    path.shift();
                                    path.shift();
                                    var paramName = prompt("New param name: ");
                                    if (paramName != undefined) {
                                        v_viewmodel.filterBuilder.changeParamNameOfFilterRequest([0], path, paramName);
                                        setTimeout(refresh, 0);
                                    }
                                }
                            }
                        };
                    }
                }
            }
        });

        v_tree.on("redraw.jstree", function(e, data) {
            $("#" + v_id).height("auto");
            $("#" + v_id).width("auto");
        });

        v_tree.on("after_open.jstree after_close.jstree", function(e, data) {
            $("#" + v_id).height("auto");
            $("#" + v_id).width("auto");
            saveOpenNodes();
            setTimeout(function() {
                highlight(data.node);
            }, 0);
        });

        v_tree.on("copy_node.jstree", function(e, data) {
            v_tree.jstree("delete_node", data.node.id);
            var toPath = v_jsTreeUtils.getPath(v_tree.jstree("get_node", data.parent)).path;

            if (v_jsTreeUtils.isNodeFromTree(data.original, v_helpTree)) {
                helpNodeCopied(data.original.id, data.parent, data.position);
            } else {
                v_parent.relativeToEvent(data.original, v_playlistId);
            }
        });

        v_tree.on("hover_node.jstree", function(e, data) {
            if (data.node.data != undefined && data.node.data.tooltip != undefined) {
                $("#" + data.node.id).prop("title", data.node.data.tooltip);
            }
        });

        openNodes();
        $("#" + v_id + "_Name").text(v_viewmodel.getName());
        setTimeout(highlight, 0);
    }

    function saveOpenNodes() {
        v_desktopData.openNodes = v_jsTreeUtils.findOpenNodes(v_tree);
    }

    function openNodes() {
        v_jsTreeUtils.openNodes(v_tree, v_desktopData.openNodes);
    }

    function highlight(node) {
        var nodes = v_tree.jstree("get_json", node, {"flat": true});
        for (var i = 0; i < nodes.length; ++i) {
            $("#" + nodes[i].id + "_anchor").removeClass("NodeFiler NodeSet NodeWarning");
            if (nodes[i].data.highlight != undefined) {
                $("#" + nodes[i].id + "_anchor").addClass("Node" + nodes[i].data.highlight);
            }
        }
    }

    function dragFromHelpValidate(p_helpId, p_requestParentId) {
        var helpPath = v_jsTreeUtils.getPath(v_helpTree.jstree("get_node", p_helpId)).path;
        if (p_requestParentId != "#") {
            var requestPath = v_jsTreeUtils.getPath(v_tree.jstree("get_node", p_requestParentId)).path;
            if (requestPath.length == 1 && requestPath[0] == 3) {
                return v_viewmodel.requestBuilder.isValidToCreateRequest(helpPath);
            } else if (requestPath.length > 1 && requestPath[0] == 3) {
                requestPath.shift();
                return v_viewmodel.requestBuilder.isValidToAddRequest(helpPath, requestPath);
            } else if (requestPath.length > 2 && requestPath[0] == 4 && requestPath[1] == 3) {
                requestPath.shift();
                requestPath.shift();
                requestPath.shift();
                return v_viewmodel.filterBuilder.isValidToConvertFilterToRequest([0], requestPath, helpPath);
            }
        }
        return false;
    }

    function helpNodeCopied(p_helpId, p_requestParentId, p_position) {
        var helpPath = v_jsTreeUtils.getPath(v_helpTree.jstree("get_node", p_helpId)).path;
        if (p_requestParentId != "#") {
            var requestPath = v_jsTreeUtils.getPath(v_tree.jstree("get_node", p_requestParentId)).path;
            var originalRequestPath = mcopy(requestPath);
            if (requestPath.length == 1 && requestPath[0] == 3) {
                requestPath.shift();
                v_viewmodel.requestBuilder.createRequest(helpPath, p_position);
                v_desktopData.openNodes.push(originalRequestPath);
                setTimeout(refresh, 0);
            } else if (requestPath.length > 1 && requestPath[0] == 3) {
                requestPath.shift();
                v_viewmodel.requestBuilder.addChildRequest(helpPath, requestPath, p_position, v_parent.altToggle);
                v_desktopData.openNodes.push(originalRequestPath);
                setTimeout(refresh, 0);
            } else if (requestPath.length > 2 && requestPath[0] == 4 && requestPath[1] == 3) {
                requestPath.shift();
                requestPath.shift();
                requestPath.shift();
                v_viewmodel.filterBuilder.convertFilterPartToRequest([0], requestPath, helpPath);
                v_desktopData.openNodes.push(originalRequestPath);
                setTimeout(refresh, 0);
            }
        }
    }

    function RelativeToConnection(p_playlistId) {
        this.getOffset = function() {
            var htmlObj = $("#" + v_id);
            var offset = htmlObj.offset();
            offset.top += htmlObj.height() / 2;
            return offset;
        };

        this.getZIndex = function() {
            return v_zIndex + 1;
        };

        this.isEnabled = function() {
            return true;
        };

        this.object = v_this;

        this.endpointType = "target";
        this.identifier = p_playlistId;
    }

    var v_relativeToSource = {
        "getOffset": function() {
            var htmlObj;

            if (!v_desktopData.visible) {
                var id = v_id + "_Header";
                htmlObj = $("#" + id);
            } else {
                var id = v_jsTreeUtils.getLastNodeIdFromPath(v_tree, [0]);
                htmlObj = $("#" + id + "_anchor");
            }

            var offset = htmlObj.offset();
            offset.left += htmlObj.width();
            offset.top += htmlObj.height() / 2;
            return offset;
        },

        "getZIndex": function() {
            return v_zIndex + 1;
        },

        "isEnabled": function() {
            return true;
        },

        "object": v_this
    }

    this.getEndpoint = function(identifier) {
        if (identifier == v_playlistId) {
            return v_relativeToSource;
        } else {
            return undefined;
        }
    };

    this.getEndpoints = function() {
        var connections = [];
        var relativeTo = v_viewmodel.getRelativeTo();
        for (var i = 0; i < relativeTo.length; ++i) {
            connections.push(new RelativeToConnection(relativeTo[i]));
        }
        return connections;
    };
}