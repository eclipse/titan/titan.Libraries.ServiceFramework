// Copyright (c) 2000-2023 Ericsson Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function PlaylistEditor_EditorContainer_View(p_viewModel, p_parent) {
    "use strict";

    var v_this = this;

    var v_viewmodel = p_viewModel;
    var v_parent = p_parent;
    var v_dataSourceUtils = new DataSourceUtils();

    var v_filterEditor;
    var v_helpTree;
    var v_views = {};
    var v_connectionsView = new GuiEditor_Connections_View("PlaylistEditor_Playground", v_this);

    this.ctrlToggle = false;
    this.altToggle = false;

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.applicationCreated = function() {
        var aligner = new CView_Aligner([], "PlaylistEditor_RequestEditorSplit", undefined, {
            "existing": true,
            "resizable": true
        });
        aligner.applicationCreated();

        v_helpTree = $("#PlaylistEditor_HelpTree");
        createHelpJSTree(v_viewmodel.getHelp());

        setupCallback();
    };

    this.setFocusedObj = function(obj) {
        v_parent.setFocusedObj(obj);
        v_connectionsView.refreshConnections();
    };

    this.fullRefresh = function() {
        for (var id in v_views) {
            v_views[id].destroy();
        }
        v_views = {};
        var editorViewmodels = v_viewmodel.getEditorViewmodels();
        for (var id in editorViewmodels) {
            v_views[id] = new PlaylistEditor_PlaylistItem_View(id, editorViewmodels[id], v_this, v_helpTree);
            v_views[id].applicationCreated();
        }

        v_connectionsView.fullRebuild();
        v_connectionsView.refreshConnections();
    };

    this.destroy = function() {
        $("#PlaylistEditor_ElementEditor").off("remove");
        $("#PlaylistEditor_ElementEditor").remove();
        $("#PlaylistEditor_FilterElementEditor").off("remove");
        $("#PlaylistEditor_FilterElementEditor").remove();
        v_connectionsView.destroy();
        $(document).off("keydown", toggle);
        $(document).off("keyup", toggle);
    };

    ///////////////////// CREATING VIEW ELEMENTS AND INTERACTIONS //////////////////////////////

    function createHelpJSTree(help) {
        var data = v_dataSourceUtils.convertHelpToTreeDataArray(help.sources);
        v_helpTree.jstree("destroy");
        v_helpTree = v_helpTree.jstree({
            "core": {
                "data": data,
                "check_callback" : function(operation, node, node_parent, node_position, more) {
                     if (operation === "copy_node" || operation === "move_node") {
                         return false;
                     } else {
                         return true;
                     }
                },
                "multiple" : false,
                "animation": false,
                "worker": false
            },
            "plugins" : ["search", "dnd"],
            "search" : {
                "show_only_matches": true
            },
            "dnd": {
                "always_copy": true
            }
        });

        v_helpTree.bind("hover_node.jstree", function(e, data) {
            $("#"+data.node.id).prop("title", data.node.data);
        });
    }

    function createEditorView() {
        var viewmodelObject = v_viewmodel.createEditorViewmodel();
        v_views[viewmodelObject.id] = new PlaylistEditor_PlaylistItem_View(viewmodelObject.id, viewmodelObject.viewmodel, v_this, v_helpTree);
        v_views[viewmodelObject.id].applicationCreated();
    }

    this.deleteEditorView = function(id) {
        v_views[id].destroy();
        delete v_views[id];
        v_viewmodel.deleteEditorViewmodel(id);
        v_connectionsView.fullRebuild();
        v_connectionsView.refreshConnections();
    };

    function setupCallback() {
        $("#PlaylistEditor_HelpSearch").on("input", function() {
            v_helpTree.jstree("search", $(this).val());
        });

        $("#PlaylistEditor_Button_Add").click(createEditorView);

        $(document).on("keydown", toggle);
        $(document).on("keyup", toggle);
    }

    function toggle(event) {
        if (event.keyCode == 17) {
            if (event.type == "keydown") {
                v_this.ctrlToggle = true;
            } else {
                v_this.ctrlToggle = false;
            }
        } else if (event.keyCode == 18) {
            if (event.type == "keydown") {
                v_this.altToggle = true;
            } else {
                v_this.altToggle = false;
            }
            event.preventDefault();
            event.stopPropagation();
        }
    }

    this.relativeToEvent = function(nodeId, to) {
        for (var id in v_views) {
            if (v_views[id].isNodeFromTree(nodeId)) {
                v_viewmodel.playlistConnection(id, to);
                break;
            }
        }
        v_connectionsView.fullRebuild();
        v_connectionsView.refreshConnections();
    };

    this.openElementEditor = function(request, offset, refresh) {
        v_viewmodel.elementEditorViewmodel.setRequest(request);
        $("#PlaylistEditor_ElementEditor").remove();
        var customData = {
            "headerText": "Edit request",
            "closeable": true,
            "draggable": true,
            "offset": offset,
            "editorOptions": {
                "disable_array_reorder": true,
                "disable_edit_json": true,
                "disable_collapse": true,
                "no_additional_properties": true
            },
            "css": {
                "width": "600px",
                "height": "506px",
                "z-index": 2000
            }
        }
        var editor = new CView_JSONEditor([v_viewmodel.elementEditorViewmodel], "PlaylistEditor_ElementEditor", "PlaylistEditor_Playground", customData);
        editor.applicationCreated();
        ViewUtils.applyCss(customData, "PlaylistEditor_ElementEditor");
        editor.refresh(true);
        ViewUtils.jumpToEditor("PlaylistEditor_ElementEditor");

        $("#PlaylistEditor_ElementEditor").one("remove", refresh);
    };

    this.openFilterElementEditor = function(filter, offset, refresh) {
        v_viewmodel.filterElementEditorViewmodel.setFilter(filter);
        $("#PlaylistEditor_FilterElementEditor").remove();
        var customData = {
            "headerText": "Edit filter",
            "closeable": true,
            "draggable": true,
            "offset": offset,
            "editorOptions": {
                "disable_array_reorder": true,
                "disable_edit_json": true,
                "disable_collapse": true,
                "no_additional_properties": true
            },
            "css": {
                "width": "600px",
                "height": "506px",
                "z-index": 2000
            }
        }
        var editor = new CView_JSONEditor([v_viewmodel.filterElementEditorViewmodel], "PlaylistEditor_FilterElementEditor", "PlaylistEditor_Playground", customData);
        editor.applicationCreated();
        ViewUtils.applyCss(customData, "PlaylistEditor_FilterElementEditor");
        editor.refresh(true);
        ViewUtils.jumpToEditor("PlaylistEditor_FilterElementEditor");

        $("#PlaylistEditor_FilterElementEditor").one("remove", refresh);
    };

    this.openFilterEditor = function(viewmodel, requestPath, requestTree, offset, refresh) {
        if (v_filterEditor != undefined) {
            v_filterEditor.destroy();
        }
        v_filterEditor = new PlaylistEditor_FilterEditor_View(viewmodel, v_this, requestPath, requestTree, v_helpTree, offset);
        v_filterEditor.applicationCreated();
        $("#PlaylistEditor_FilterEditor").one("remove", refresh);
    };

    this.getObjectsToConnect = function() {
        var list = [];
        for (var id in v_views) {
            list.push(v_views[id]);
        }
        return list;
    };

    this.getEndpoint = function(connectionType, identifier) {
        for (var id in v_views) {
            var endpoint = v_views[id].getEndpoint(identifier);
            if (endpoint != undefined) {
                return endpoint;
            }
        }
        alert("No endpoint found for " + identifier);
    };

    this.refreshConnections = v_connectionsView.refreshConnections;
}
