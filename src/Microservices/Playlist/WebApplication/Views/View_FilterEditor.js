// Copyright (c) 2000-2023 Ericsson Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function PlaylistEditor_FilterEditor_View(p_viewmodel, p_parent, p_requestPath, p_requestTree, p_helpTree, p_offset) {

    var v_viewmodel = p_viewmodel;
    var v_id = "PlaylistEditor_FilterEditor";
    var v_parent = p_parent;
    var v_editorDiv;

    var v_tree;
    var v_requestPath = p_requestPath;
    var v_requestTree = p_requestTree;
    var v_helpTree = p_helpTree;
    var v_offset = p_offset;

    var v_jsTreeUtils = new JsTreeUtils();

    var v_this = this;
    var v_editorOpen = false;

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.applicationCreated = function() {
        var html = '' +
        '<div id="' + v_id + '" class="PlaylistEditor_FilterEditor">' +
            '<div id="' + v_id + '_Header' + '" class="PlaylistEditor_EditorHeader">' +
                '<button id="' + v_id + '_Button_Add" class="PlaylistEditor_EditorButtonLeft">Create</button>' +
                '<label id="' + v_id + '_HeaderText" class="PlaylistEditor_FilterEditorHeaderLabel">Edit filter</label>' +
                '<button id="' + v_id + '_Button_Context" class="PlaylistEditor_EditorButtonRight"><img src="WebApplicationFramework/Res/grip_white.png"></button>' +
                '<button id="' + v_id + '_Button_Close" class="PlaylistEditor_EditorButtonRight">X</button>' +
            '</div>' +
            '<div id="' + v_id + '_Tree"></div>' +
        '</div>';

        $("#PlaylistEditor_Playground").append(html);
        v_tree = $("#" + v_id + "_Tree");
        createTree();

        v_editorDiv = $("#" + v_id);
        v_editorDiv.draggable({
            stop: function(event, ui) {
                document.getElementById(v_id).style.height = "auto";
                document.getElementById(v_id).style.width = "auto";
            },
            handle: "#" + v_id + "_Header",
            containment: [$("#PlaylistEditor_Playground").offset().left, $("#PlaylistEditor_Playground").offset().top, 20000, 20000]
        });
        v_editorDiv.on('click', function(){
            v_parent.setFocusedObj(v_this);
        });
        v_editorDiv.on('dragstart', function(){
            v_parent.setFocusedObj(v_this);
        });

        v_editorDiv.offset(p_offset);
        v_editorDiv.offset(p_offset);

        setupCallbacks();
        v_parent.setFocusedObj(v_this);
    };

    this.destroy = function() {
        closeEditors();
        v_editorDiv.remove();
    };

    this.setDefaultZidx = function() {
        v_editorDiv.css("z-index", 1500);
    };

    this.setFocusedObj = v_parent.setFocusedObj;

    this.setZidx = function() {
        v_editorDiv.css("z-index", 1500);
    };

    this.deletePressed = function() {
        var selected = v_tree.jstree("get_selected")[0];
        if (selected.length != undefined) {

            var filterPath = v_jsTreeUtils.getPath(v_tree.jstree("get_node", selected)).path;
            filterPath.shift();
            closeEditors();
            v_viewmodel.requestBuilder.deleteFilterPart(v_requestPath, filterPath);
            setTimeout(createTree, 0);
        }
    };

    ///////////////////// CREATING AND REFRESHING THE TREE //////////////////////////////

    function closeEditors() {
        $("#PlaylistEditor_FilterElementEditor").remove();
    }

    function createTree() {
        var data = [];
        v_viewmodel.traverseFilter(v_viewmodel.requestBuilder.getFilterPart(v_requestPath, []), data);
        v_tree.jstree("destroy");
        v_tree = v_tree.jstree({
            "core": {
                "data": data,
                "check_callback": function(operation, node, node_parent, node_position, more) {
                    if (v_editorOpen) {
                        return false;
                    } else if (operation === "copy_node" && v_jsTreeUtils.isNodeFromTree(node, v_helpTree) && !v_jsTreeUtils.isRoot(node_parent)) {
                        return dragFromHelpValidate(node.id, node_parent.id);
                    } else if (operation === "copy_node" && v_jsTreeUtils.isNodeFromTree(node, v_requestTree) && !v_jsTreeUtils.isRoot(node_parent)) {
                        var path = v_jsTreeUtils.getPath(v_requestTree.jstree("get_node", node.id)).path;
                        if (path.length > 1 && path[0] == 3) {
                            path.shift();
                            return hasPrefix(v_requestPath, path);
                        } else {
                            return false;
                        }
                    } else if (operation === "copy_node") {
                        return false;
                    } else {
                        return true;
                    }
                },
                "multiple": false,
                "animation": false,
                "worker": false
            },
            "plugins": ["contextmenu", "dnd"],
            "dnd": {
                "always_copy": true
            },
            "contextmenu": {
                "items": function($node) {
                    if (v_editorOpen) {
                        return {};
                    } else {
                        return {
                            "Edit": {
                                "label": "Edit filter",
                                "action": function(data) {
                                    v_editorOpen = true;
                                    var offset = data.reference.offset();
                                    offset.left += data.reference.width();
                                    var filterPath = v_jsTreeUtils.getPath(v_tree.jstree("get_node", data.reference)).path;
                                    filterPath.shift();
                                    v_parent.openFilterElementEditor(v_viewmodel.requestBuilder.getFilterPart(v_requestPath, filterPath), offset, function() {
                                        v_editorOpen = false;
                                        setTimeout(createTree, 0);
                                    });
                                },
                                "separator_after": true
                            },
                            "Add": {
                                "label": "Add param",
                                "action": function(data) {
                                    var node = v_tree.jstree("get_node", data.reference);
                                    var filterPath = v_jsTreeUtils.getPath(node).path;
                                    var position = v_tree.jstree("get_children_dom", node).length;
                                    filterPath.shift();
                                    filterPath.push(position);
                                    if (v_viewmodel.requestBuilder.isValidToAddParamToFilterRequest(v_requestPath, filterPath)) {
                                        var paramName = prompt("New param name: ");
                                        if (paramName != undefined) {
                                            closeEditors();
                                            v_viewmodel.requestBuilder.addFilterPart(v_requestPath, filterPath, paramName);
                                            setTimeout(createTree, 0);
                                        }
                                    } else {
                                        alert('Cannot add param to a dataValue');
                                    }
                                }
                            },
                            "RemapTo": {
                                "label": "Add remapTo",
                                "action": function(data) {

                                    var node = v_tree.jstree("get_node", data.reference);
                                    var filterPath = v_jsTreeUtils.getPath(node).path;
                                    var position = v_tree.jstree("get_children_dom", node).length;
                                    filterPath.shift();
                                    filterPath.push(position);
                                    if (v_viewmodel.requestBuilder.isValidToAddParamToFilterRequest(v_requestPath, filterPath)) {
                                        var paramName = "remapTo";
                                        closeEditors();
                                        v_viewmodel.requestBuilder.addFilterPart(v_requestPath, filterPath, paramName);
                                        setTimeout(createTree, 0);
                                    } else {
                                        alert('Cannot add param to a dataValue');
                                    }
                                }
                            },
                            "Delete": {
                                "label": "Delete",
                                "action": function(data) {
                                    var filterPath = v_jsTreeUtils.getPath(v_tree.jstree("get_node", data.reference)).path;
                                    filterPath.shift();
                                    closeEditors();
                                    v_viewmodel.requestBuilder.deleteFilterPart(v_requestPath, filterPath);
                                    setTimeout(createTree, 0);
                                },
                                "separator_after": true
                            },
                            "ChangeParamName": {
                                "label": "Change param name",
                                "action": function(data) {
                                    var filterPath = v_jsTreeUtils.getPath(v_tree.jstree("get_node", data.reference)).path;
                                    filterPath.shift();
                                    var paramName = prompt("New param name: ");
                                    if (paramName != undefined) {
                                        closeEditors();
                                        v_viewmodel.requestBuilder.changeParamNameOfFilterRequest(v_requestPath, filterPath, paramName);
                                        setTimeout(createTree, 0);
                                    }
                                }
                            }
                        };
                    }
                },
                "select_node": true
            }
        });

        v_tree.bind("hover_node.jstree", function(e, data) {
            var filterPath = v_jsTreeUtils.getPath(v_tree.jstree("get_node", data.node.id)).path;
            filterPath.shift();
            var string = JSON.stringify(v_viewmodel.requestBuilder.getFilterPartCopy(v_requestPath, filterPath), null, 4);
            $("#" + data.node.id).prop("title", string);
        });

        v_tree.bind("select_node.jstree", function (e, data) {
            closeEditors();
        });

        v_tree.bind("after_close.jstree", function (e, data) {
            v_tree.jstree("open_all");
        });

        v_tree.bind("redraw.jstree", function(e, data) {
            document.getElementById(v_id).style.height = "auto";
            document.getElementById(v_id).style.width = "auto";
        });

        v_tree.bind("copy_node.jstree", function(e, data) {
            if (v_jsTreeUtils.isNodeFromTree(data.original, v_helpTree)) {
                helpNodeCopied(data);
            } else if (v_jsTreeUtils.isNodeFromTree(data.original, v_requestTree)) {
                requestNodeCopied(data);
            }
        });

        v_tree.bind("ready.jstree", function(e, data) {
            v_tree.jstree("open_all");
        });
    }

    ///////////////////// HANDLING EVENTS //////////////////////////////

    function dragFromHelpValidate(p_helpId, p_filterId) {
        var helpPath = v_jsTreeUtils.getPath(v_helpTree.jstree("get_node", p_helpId)).path;
        var filterPath = v_jsTreeUtils.getPath(v_tree.jstree("get_node", p_filterId)).path;
        filterPath.shift();
        return v_viewmodel.requestBuilder.isValidToConvertFilterToRequest(v_requestPath, filterPath, helpPath)
    }

    function helpNodeCopied(data) {
        v_tree.jstree("delete_node", data.node.id);
        var helpPath = v_jsTreeUtils.getPath(v_helpTree.jstree("get_node", data.original.id)).path;
        var filterPath = v_jsTreeUtils.getPath(v_tree.jstree("get_node", data.parent)).path;
        filterPath.shift();
        closeEditors();
        v_viewmodel.requestBuilder.convertFilterPartToRequest(v_requestPath, filterPath, helpPath);
        setTimeout(createTree, 0);
    }

    function requestNodeCopied(data) {
        v_tree.jstree("delete_node", data.node.id);
        var path = v_jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.original.id)).path;
        path.shift();
        var dataValue = "%Parent" + (path.length - 1) + "%";
        var filterPath = v_jsTreeUtils.getPath(v_tree.jstree("get_node", data.parent)).path;
        filterPath.shift();
        closeEditors();
        v_viewmodel.requestBuilder.convertFilterPartToDataValue(v_requestPath, filterPath, dataValue);
        setTimeout(createTree, 0);
    }

    function setupCallbacks() {
        $('#' + v_id + '_Button_Close').click(v_this.destroy);
        $('#' + v_id + '_Button_Context').click(function() {
            $("#" + v_jsTreeUtils.getNodeIdFromPath(v_tree, [0]) + " > a").contextmenu();
        });
        $('#' + v_id + '_Button_Add').click(function() {
            closeEditors();
            v_viewmodel.requestBuilder.addFilterPart(v_requestPath, []);
            setTimeout(createTree, 0);
        });
    }
}
//# sourceURL=PlaylistEditor\Views\View_FilterEditor.js