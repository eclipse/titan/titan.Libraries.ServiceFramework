#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
DSHELP = [
    {
        "dataElement": {
            "description": "Help on the available elements.",
            "name": "help",
            "valueType": "octetstringType"
        }
    },
    {
        "dataElement": {
            "description": "List of available playlists.",
            "name": "Playlists",
            "valueType": "charstringlistType",
            "typeDescriptor": {
                "isListOf": True,
                "typeName": "Playlist"
            }
        }
    },
    {
        "dataElement": {
            "description": "The status of the playlist.",
            "name": "Status",
            "valueType": "statusLEDType",
            "parameters": [
                {
                    "description": "The name of the playlist.",
                    "exampleValue": "group28/playlist",
                    "name": "Playlist",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Playlist"
                        }
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Start the playlist.",
            "name": "Start",
            "valueType": "intType",
            "parameters": [
                {
                    "description": "The name of the playlist.",
                    "exampleValue": "group28/playlist",
                    "name": "Playlist",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Playlist"
                        }
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Stop a running playlist.",
            "name": "Stop",
            "valueType": "intType",
            "parameters": [
                {
                    "description": "The name of the playlist.",
                    "exampleValue": "group28/playlist",
                    "name": "Playlist",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Playlist"
                        }
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Pause a running playlist.",
            "name": "Pause",
            "valueType": "intType",
            "parameters": [
                {
                    "description": "The name of the playlist.",
                    "exampleValue": "group28/playlist",
                    "name": "Playlist",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Playlist"
                        }
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "Delete the playlist.",
            "name": "Delete",
            "valueType": "intType",
            "parameters": [
                {
                    "description": "The name of the playlist.",
                    "exampleValue": "group28/playlist",
                    "name": "Playlist",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Playlist"
                        }
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "The identifier playlist file.",
            "name": "Edit",
            "valueType": "charstringType",
            "typeDescriptor": {
                "isListOf": False,
                "typeName": "Playlist"
            },
            "parameters": [
                {
                    "description": "The name of the playlist.",
                    "exampleValue": "group28/playlist",
                    "name": "Playlist",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Playlist"
                        }
                    }
                }
            ]
        }
    },
    {
        "dataElement": {
            "description": "The json descriptor of the playlist.",
            "name": "Descriptor",
            "valueType": "charstringType",
            "parameters": [
                {
                    "description": "The name of the playlist.",
                    "exampleValue": "group28/playlist",
                    "name": "Playlist",
                    "typeDescriptor": {
                        "reference": {
                            "typeName": "Playlist"
                        }
                    }
                }
            ]
        }
    }
]