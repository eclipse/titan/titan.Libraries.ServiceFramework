#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
'''
Performace Monitor tool to show process and system statistics.
The measurment script used can be configured via config.json.

The script output is redirected as follows:
 stdout: <workspace>/console.out
 stderr: <workspace>/stderr.out
 
(The <workspace> is the actual working directory of the script)

The stdout output is shown as Process Statistics
The stderr output is shown as Information about the script
If the script produces <workspace>/sysstat.out it will be
shown as System Statistics.

The recommended measurement script to use is sysstatLogger.sh of CLL/tools.
'''

from PerformanceMonitor import PerformanceMonitor

EXTENSION = 'api.performancemonitor'

def createHandler(directory, *args):
    return PerformanceMonitor(directory)
