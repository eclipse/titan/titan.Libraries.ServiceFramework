#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
import sys, json, os

destination = sys.argv[1]
microservicesDir = destination + '/Microservices'

os.system('rm -rf ' + destination)
if not os.path.exists(destination):
    os.makedirs(destination)

os.system('cp -ar ../../src/AppAgent.py ' + destination)
os.system('cp -ar ../../src/LogConfig.json ' + destination)
os.makedirs(microservicesDir)
os.system('cp -ar ../../src/Common ' + destination)
os.system('cp -ar ../../src/Microservices/Authentication ' + microservicesDir)
os.system('cp -ar ../../src/Microservices/DataSource ' + microservicesDir)
os.system('cp -ar ../../lib/WebGUI/ ' + destination)
#os.system('cp -ar ../../src/WebGUI/src/DsRestAPI/Api/Js ' + destination + '/WebGUI/htdocs/Utils/DsRestAPI')
os.system('cp -ar ../Microservices/BattleShip ' + microservicesDir)
os.system('cp -ar ../Microservices/GuiConfigHandlerForBattleship ' + microservicesDir)
os.system('cp -ar ../Microservices/__init__.py ' + microservicesDir)

with open(destination + '/config.json', 'w') as f:
    config = {
        "address": "",
        "port": 8000,
        "htdocs": "WebGUI/htdocs",
        "ssl": False
    }
    json.dump(config, f)

