#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
import os, json, traceback, sys
from Common.BaseGuiConfigHandler import BaseGuiConfigHandler

class GuiConfigHandler(BaseGuiConfigHandler):

    def createConfig(self, userGroups):
        apps = []
        default = None

        apps.append({
            'directory': 'WebApplications/CustomizableApp',
            'name': 'BattleShip',
            'icon': 'CustomizableContent/BattleShip/Res/main_icon.png',
            'params': {
                'customization': 'CustomizableContent/BattleShip'
            }
        })

        apps.append({
            'directory': 'WebApplications/Authentication',
            'name': 'Logout',
            'icon': 'WebApplications/Authentication/logout.png',
            'params': {
                'logout': True
            }
        })

        return {'availableApps': apps, 'defaultApp': 'BattleShip'}