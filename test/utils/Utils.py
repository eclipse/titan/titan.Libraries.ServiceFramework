#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
ADMIN = {
    "username": "admin",
    "password": "admin",
    "groups": set(["admin"])
}

NO_USER = {
    "username": None,
    "password": None,
    "groups": set([])
}

WRONG_USER = {
    'username': 'not_admin',
    'password': 'not_admin_password',
    'groups': set(['not_admin_group'])
}

GROUPRIGHTS_DIR = 'test/utils/MockedRights'
DEFAULT_RIGHTS = ['a']

PLAYLIST_DIR = 'test/utils/MockedPlaylist'

HTTPSERVER_DIR = 'test/utils/HttpServerDir'

def getEmptyResponse():
    return {
        'returnCode': 200,
        'mimeType': 'text/plain',
        'body': '',
        'headers': {}
    }