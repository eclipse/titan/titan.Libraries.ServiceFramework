#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
import unittest, json
import Microservices.Authentication
from test.utils.Utils import *

class AuthenticationHandlerTest(unittest.TestCase):

    def setUp(self):
        # the "microservice" under test
        self.handler = Microservices.Authentication.createHandler('src/Microservices/Authentication')
        # the datasource handlers of the microservice
        self.datasourceHandlers = self.handler.getDataSourceHandlers()
        # mocked usar "database"
        self.handler._userHandler._userGroups = {
            "users": {
                "admin": [
                    "admin"
                ]
            },
            "groups": [
                "admin"
            ]
        }
        # we do not save the "database"
        self.handler._userHandler._saveUserGroups = lambda *args: None

    def tearDown(self):
        # close tested "microservice"
        self.handler.close()

    def test_authentication(self):
        '''
        Author: EDNIGBO Daniel Gobor
        Testcase: test_authentication
        Tested component: ServiceFramework
        Feature: ServiceFramework - Authentication
        Requirement: authentication and session handling
        Action_To_Be_taken:
            check empty credentials if there is no cookie
            login
            check set cookie header
            check user credentials with valid cookie
            logout
            check logout success
            check user credentials with old cookie
        Expected_Result: pass
        '''

        # check empty credentials if there is no cookie
        credentials = self.handler.getUserCredentials({'cookie': None})
        self.assertEqual(credentials, NO_USER)
        # login
        response = getEmptyResponse()
        self.handler.handleMessage('POST', 'login/api.authenticate', {}, '{"username": "admin", "password": "admin"}', NO_USER, response)
        # check set cookie header
        self.assertEqual(200, response['returnCode'])
        self.assertIn('Set-Cookie', response['headers'])
        self.assertIn('PYSESSID=', response['headers']['Set-Cookie'])
        self.assertIn('Path=/;', response['headers']['Set-Cookie'])
        self.assertIn('expires=', response['headers']['Set-Cookie'])
        # check user credentials with valid cookie
        cookie = response['headers']['Set-Cookie'].split(';')[0] + ';'
        credentials = self.handler.getUserCredentials({'cookie': cookie})
        self.assertEqual(credentials['username'], 'admin')
        self.assertEqual(credentials['password'], 'admin')
        # logout
        response = getEmptyResponse()
        self.handler.handleMessage('POST', 'logout/api.authenticate', {'cookie': cookie}, '', credentials, response)
        # check logout success
        self.assertEqual(200, response['returnCode'])
        self.assertIn('Set-Cookie', response['headers'])
        self.assertIn('PYSESSID=', response['headers']['Set-Cookie'])
        self.assertIn('Path=/;', response['headers']['Set-Cookie'])
        self.assertIn('expires=Thu, 01 Jan 1970 00:00:00 UTC;', response['headers']['Set-Cookie'])
        # check user credentials with old cookie
        credentials = self.handler.getUserCredentials({'cookie': cookie})
        self.assertEqual(credentials, NO_USER)

    def test_userHandling(self):
        '''
        Author: EDNIGBO Daniel Gobor
        Testcase: test_userHandling
        Tested component: ServiceFramework
        Feature: ServiceFramework - Authentication
        Requirement: user handling
        Action_To_Be_taken:
            create 'test_group' group
            check if 'test_group' exists
            add 'test_user[1-3]' users to 'test_group'
            check if the users are in 'test_group'
            check the groups of 'test_user2'
            remove 'test_user2' from the 'test_group'
            check if 'test_user2' was removed from 'test_group'
            check the groups of 'test_user2'
            delete 'test_group'
            check if 'test_group' was deleted
        Expected_Result: pass
        '''

        # create 'test_group' group
        self.set('AddGroup', 'test_group', 4)
        # check if 'test_group' exists
        groups = [element['node']['val'] for element in self.get('ListGroups')['list']]
        self.assertIn('test_group', groups)
        # add 'test_user[1-3]' users to 'test_group'
        self.set('AddUserToGroup', 'test_user1', 4, None, 'test_group')
        self.set('AddUserToGroup', 'test_user2', 4, None, 'test_group')
        self.set('AddUserToGroup', 'test_user3', 4, None, 'test_group')
        # check if the users are in 'test_group'
        users = [element['node']['val'] for element in self.get('ListUsersInGroup', None, 'test_group')['list']]
        self.assertIn('test_user1', users)
        self.assertIn('test_user2', users)
        self.assertIn('test_user3', users)
        self.assertEqual(3, len(users))
        # check the groups of 'test_user2'
        groupsOfUser = [element['node']['val'] for element in self.get('ListGroupsOfUser', 'test_user2')['list']]
        self.assertIn('test_group', groupsOfUser)
        self.assertEqual(1, len(groupsOfUser))
        # remove 'test_user2' from the 'test_group'
        self.set('RemoveUserFromGroup', '', 1, 'test_user2', 'test_group')
        # check if 'test_user2' was removed from 'test_group'
        users = [element['node']['val'] for element in self.get('ListUsersInGroup', None, 'test_group')['list']]
        self.assertIn('test_user1', users)
        self.assertNotIn('test_user2', users)
        self.assertIn('test_user3', users)
        self.assertEqual(2, len(users))
        # check the groups of 'test_user2'
        groupsOfUser = [element['node']['val'] for element in self.get('ListGroupsOfUser', 'test_user2')['list']]
        self.assertEqual(0, len(groupsOfUser))
        # delete 'test_group'
        self.set('RemoveGroup', '', 1, None, 'test_group')
        # check if 'test_group' was deleted
        groups = [element['node']['val'] for element in self.get('ListGroups')['list']]
        self.assertNotIn('test_group', groups)
        groupsOfUser = [element['node']['val'] for element in self.get('ListGroupsOfUser', 'test_user1')['list']]
        self.assertEqual(0, len(groupsOfUser))

    def test_groupHandling(self):
        '''
        Author: EDNIGBO Daniel Gobor
        Testcase: test_userHandling
        Tested component: ServiceFramework
        Feature: ServiceFramework - Authentication
        Requirement: user group handling
        Action_To_Be_taken:
            add 'test_group' group
            add 'admin' user to the group
            login as admin
            check if credentials are correct
        Expected_Result: pass
        '''

        # add 'test_group' group
        self.set('AddGroup', 'test_group', 4)
        # add 'admin' user to the group
        self.set('AddUserToGroup', 'admin', 4, None, 'test_group')
        # login as admin
        response = getEmptyResponse()
        self.handler.handleMessage('POST', 'login/api.authenticate', {}, '{"username": "admin", "password": "admin"}', NO_USER, response)
        # check if credentials are correct
        cookie = response['headers']['Set-Cookie'].split(';')[0] + ';'
        credentials = self.handler.getUserCredentials({'cookie': cookie})
        self.assertEqual(credentials['username'], 'admin')
        self.assertEqual(credentials['password'], 'admin')
        self.assertIn('test_group', credentials['groups'])

    def test_help(self):
        '''
        Author: EDNIGBO Daniel Gobor
        Testcase: test_help
        Tested component: ServiceFramework
        Feature: ServiceFramework - Authentication
        Requirement: datasource help in Authentication
        Action_To_Be_taken:
            get help
            check help syntax
        Expected_Result: pass
        '''

        # get help
        help = self.get('help')['node']['val']
        # check help syntax
        json.loads(help.decode('hex'))

    def get(self, element, user = None, group = None):
        request = {
            'source': self.handler.SOURCE_ID,
            'element': element,
            'params': []
        }
        if user is not None:
            request['params'].append({
                'paramName': 'User',
                'paramValue': user
            })
        if group is not None:
            request['params'].append({
                'paramName': 'Group',
                'paramValue': group
            })
        return self.datasourceHandlers[self.handler.SOURCE_ID]['getDataHandler'](request, ADMIN)

    def set(self, element, content, tp, user = None, group = None):
        request = {
            'source': self.handler.SOURCE_ID,
            'element': element,
            'params': [],
            'tp': tp,
            'content': content
        }
        if user is not None:
            request['params'].append({
                'paramName': 'User',
                'paramValue': user
            })
        if group is not None:
            request['params'].append({
                'paramName': 'Group',
                'paramValue': group
            })
        return self.datasourceHandlers[self.handler.SOURCE_ID]['setDataHandler'](request, ADMIN)
