#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
import unittest, json
from Common.BaseGuiConfigHandler import BaseGuiConfigHandler
from test.utils.Utils import *

class DummyGuiConfigHandler(BaseGuiConfigHandler):

    def createConfig(self, userGroups):
        apps = BaseGuiConfigHandler.createConfig(self, userGroups)
        if self.hasAccessRight('b', userGroups):
            apps['availableApps'].insert(0, {'directory': 'WebApplications/B'})
        if self.hasAccessRight('a', userGroups):
            apps['availableApps'].insert(0, {'directory': 'WebApplications/A'})
        return apps


class AuthenticationHandlerTest(unittest.TestCase):

    def setUp(self):
        # the "microservice" under test
        self.handler = DummyGuiConfigHandler(GROUPRIGHTS_DIR)
        # change default rights so we can test access right handling
        self.handler._groupRightsHandler._default = DEFAULT_RIGHTS
        # the datasource handlers of the microservice
        self.datasourceHandlers = self.handler.getDataSourceHandlers()

    def tearDown(self):
        # close tested "microservice"
        self.handler.close()

    def test_loginPage(self):
        '''
        Author: EDNIGBO Daniel Gobor
        Testcase: test_loginPage
        Tested component: ServiceFramework
        Feature: ServiceFramework - BaseGuiConfigHandler
        Requirement: gui config for login page
        Action_To_Be_taken:
            get gui config without login info
            check login in returned config
        Expected_Result: pass
        '''

        # get gui config without login info
        response = getEmptyResponse()
        self.handler.handleMessage('GET', 'CustomizableContent/MainConfig.json', {}, '', NO_USER, response)
        config = json.loads(response['body'])
        # check login in returned config
        self.assertEqual(1, len(config['availableApps']))
        self.assertEqual('Login', config['availableApps'][0]['name'])

    def test_whileLoggedIn(self):
        '''
        Author: EDNIGBO Daniel Gobor
        Testcase: test_whileLoggedIn
        Tested component: ServiceFramework
        Feature: ServiceFramework - BaseGuiConfigHandler
        Requirement: gui config for a logged in user
        Action_To_Be_taken:
            get gui config with login info
            check 'A' application in returned config
            check logout in returned config
        Expected_Result: pass
        '''

        # get gui config with login info
        response = getEmptyResponse()
        self.handler.handleMessage('GET', 'CustomizableContent/MainConfig.json', {}, '', ADMIN, response)
        config = json.loads(response['body'])
        # check 'A' application in returned config
        self.assertEqual(2, len(config['availableApps']))
        self.assertEqual('WebApplications/A', config['availableApps'][0]['directory'])
        # check logout in returned config
        self.assertEqual('Logout', config['availableApps'][1]['name'])

    def test_help(self):
        '''
        Author: EDNIGBO Daniel Gobor
        Testcase: test_help
        Tested component: ServiceFramework
        Feature: ServiceFramework - BaseGuiConfigHandler
        Requirement: datasource help in BaseGuiConfigHandler
        Action_To_Be_taken:
            get help
            check help syntax
        Expected_Result: pass
        '''

        # get help
        help = self.datasourceHandlers[self.handler.SOURCE_ID]['getDataHandler']({'source': self.handler.SOURCE_ID, 'element': 'help', 'params': []}, ADMIN)['node']['val']
        # check help syntax
        json.loads(help.decode('hex'))
