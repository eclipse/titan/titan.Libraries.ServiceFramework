#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
import unittest, json
from Common.EditableGroupRights import EditableGroupRights
from test.utils.Utils import *

class EditableGroupRightsTest(unittest.TestCase):

    def setUp(self):
        # the "feature" under test
        self.handler = EditableGroupRights(GROUPRIGHTS_DIR, DEFAULT_RIGHTS)
        # we do not save the "database"
        self.handler._saveGroupRights = lambda *args: None

    def test_handlingGroupRights(self):
        '''
        Author: EDNIGBO Daniel Gobor
        Testcase: test_handlingGroupRights
        Tested component: ServiceFramework
        Feature: ServiceFramework - EditableGroupRights
        Requirement: group rights handling in ServiceFramework
        Action_To_Be_taken:
            check group rigths schema
            set group rigths of group 'test_group1' and 'test_group2'
            check group rigths of 'test_group1' via function call
            check group rigths of 'test_group1' via api call
            check handled groups
            delete 'test_group1' rights
            check group rights of 'test_group1'
            check handled groups
        Expected_Result: pass
        '''

        # check group rigths schema
        response = self.get('GroupRightsSchema')
        schema = json.loads(response['node']['val'])
        self.assertIn('$schema', schema)
        self.assertEqual('array', schema['type'])
        self.assertEqual('string', schema['items']['type'])
        self.assertEqual(3, len(schema['items']['enum']))
        # set group rigths of group 'test_group1' and 'test_group2'
        self.set('GroupRights', '["a", "b", "c"]', 'test_group1')
        self.set('GroupRights', '["a", "b"]', 'test_group2')
        # check group rigths of 'test_group1' via function call
        rights = self.handler.getGroupRights('test_group1')
        self.assertEqual(3, len(rights))
        self.assertIn('a', rights)
        self.assertIn('b', rights)
        self.assertIn('c', rights)
        # check group rigths of 'test_group1' via api call
        rights = json.loads(self.get('GroupRights', 'test_group1')['node']['val'])
        self.assertEqual(3, len(rights))
        self.assertIn('a', rights)
        self.assertIn('b', rights)
        self.assertIn('c', rights)
        # check handled groups
        groups = [element['node']['val'] for element in self.get('HandledGroups', 'test_group1')['list']]
        self.assertEqual(2, len(groups))
        self.assertIn('test_group1', groups)
        self.assertIn('test_group2', groups)
        # delete 'test_group1' rights
        self.set('GroupRights', 'null', 'test_group1')
        # check group rights of 'test_group1'
        rights = self.handler.getGroupRights('test_group1')
        self.assertEqual(1, len(rights))
        self.assertIn('a', rights)
        # check handled groups
        groups = [element['node']['val'] for element in self.get('HandledGroups', 'test_group1')['list']]
        self.assertEqual(1, len(groups))
        self.assertIn('test_group2', groups)

    def get(self, element, group = None):
        params = []
        if group is not None:
            params.append({
                'paramName': 'Group',
                'paramValue': group
            })
        return self.handler.handleGetData(element, params, ADMIN)

    def set(self, element, content, group = None):
        params = []
        if group is not None:
            params.append({
                'paramName': 'Group',
                'paramValue': group
            })
        return self.handler.handleSetData(element, params, content, ADMIN)

