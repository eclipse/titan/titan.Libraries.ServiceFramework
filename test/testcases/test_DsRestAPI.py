#// Copyright (c) 2000-2023 Ericsson Telecom AB                                                         //
#// All rights reserved. This program and the accompanying materials are made available under the terms //
#// of the Eclipse Public License v2.0 which accompanies this distribution, and is available at         //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                           //
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
import unittest
from Common.DsRestAPI import DsRestAPI
from utils.MockedHandler import *

# basic requests
INT_REQUEST = {"requests": [{"getData": {"source": "dummy", "element": "int"}}]}
STRING_REQUEST = {"requests": [{"getData": {"source": "dummy", "element": "string"}}]}
LIST_REQUEST = {"requests": [{"getData": {"source": "dummy", "element": "list"}}]}
LIST_WITH_CHILDREN = {"requests": [{"getData": {"source": "dummy", "element": "list", "children": [{"getData": {"source": "dummy", "element": "stringOfList", "params": [{"paramName": "dummyParam", "paramValue": "%Parent0%"}]}}]}}]}
LIST_WITH_SELECTION = {"requests": [{"getData": {"source": "dummy", "element": "list", "selection": [1], "children": [{"getData": {"source": "dummy", "element": "stringOfList", "params": [{"paramName": "dummyParam", "paramValue": "%Parent0%"}]}}]}}]}
NODE_WITH_CHILDREN = {"requests": [{"getData": {"source": "dummy", "element": "int", "children": [{"getData": {"source": "dummy", "element": "string"}}]}}]}
# prefilters
INT_REQUEST_CTRUE = {"requests": [{"getData": {"source": "dummy", "element": "int", "filter": {"dataValue": "true"}}}]}
INT_REQUEST_CFALSE = {"requests": [{"getData": {"source": "dummy", "element": "int", "filter": {"dataValue": "false"}}}]}
INT_REQUEST_TRUE = {"requests": [{"getData": {"source": "dummy", "element": "int", "filter": {"request": {"source": "dummy", "element": "true"}}}}]}
INT_REQUEST_FALSE = {"requests": [{"getData": {"source": "dummy", "element": "int", "filter": {"request": {"source": "dummy", "element": "false"}}}}]}
INT_REQUEST_PARAM_TRUE = {"requests": [{"getData": {"source": "dummy", "element": "int", "filter": {"request": {"source": "dummy", "element": "boolOfParam", "params": [{"paramName": "dummyParam", "paramValue": {"request": {"source": "dummy", "element": "int"}}}]}}}}]}
INT_REQUEST_PARAM_FALSE = {"requests": [{"getData": {"source": "dummy", "element": "int", "filter": {"request": {"source": "dummy", "element": "boolOfParam", "params": [{"paramName": "dummyParam", "paramValue": {"request": {"source": "dummy", "element": "string"}}}]}}}}]}
INT_REQUEST_REMAP_TRUE = {"requests": [{"getData": {"source": "dummy", "element": "int", "filter": {"request": {"source": "dummy", "element": "false", "remapTo": {"request": {"source": "dummy", "element": "true"}}}}}}]}
# postfilter
NODE_REQUEST_PARENT_FALSE = {"requests": [{"getData": {"source": "dummy", "element": "false", "filter": {"dataValue": "%Parent0%"}}}]}
NODE_REQUEST_PARENT_TRUE = {"requests": [{"getData": {"source": "dummy", "element": "true", "filter": {"dataValue": "%Parent0%"}}}]}
LIST_REQUEST_FILTERED = {"requests": [{"getData": {"source": "dummy", "element": "list", "children": [{"getData": {"source": "dummy", "element": "stringOfList", "params": [{"paramName": "dummyParam", "paramValue": "%Parent0%"}]}}], "filter": {"request": {"source": "dummy", "element": "boolOfList", "params": [{"paramName": "dummyParam", "paramValue": {"dataValue": "%Parent0%"}}]}}}}]}
# set requests
INT_REQUEST_SET = {"requests": [{"setData": {"source": "dummy", "element": "int", "tp": 1, "content": "-1"}}]}
LIST_ELEMENT_SET = {"requests": [{"setData": {"source": "dummy", "element": "stringOfList", "tp": 4, "content": "other_string", "params": [{"paramName": "dummyParam", "paramValue": "0"}]}}]}
LIST_SET = {"requests": [{"setData": {"source": "dummy", "element": "list", "tp": 8, "content": "-1", "indxsInList": [1]}}]}
# rangefilter
LIST_REQUEST_WITH_RANGEFILTER = {"requests": [{"getData": {"source": "dummy", "element": "list", "rangeFilter": {"offset": 1, "count": 1}}}]}

class DsRestAPITest(unittest.TestCase):

    def setUp(self):
        # a mocked microservice
        self.mockedHandler = MockedDsRestApiHandler()
        # the "feature" under test
        self.dsRestAPI = DsRestAPI(self.mockedHandler.mockedGetDataHandler, self.mockedHandler.mockedSetDataHandler)

    def test_getData(self):
        '''
        Author: EDNIGBO Daniel Gobor
        Testcase: test_getData
        Tested component: ServiceFramework
        Feature: ServiceFramework - DsRestAPI
        Requirement: getData request handling
        Action_To_Be_taken:
            process requests and check responses
        Expected_Result: pass
        '''

        # basic
        self.sendRequestAndCompare(INT_REQUEST, self.mockedHandler.elements['int'])
        self.sendRequestAndCompare(STRING_REQUEST, self.mockedHandler.elements['string'])

        # user credentials
        self.sendRequestAndCompare(INT_REQUEST, self.mockedHandler.elements['userError'], WRONG_USER)

        # prefilter
        self.sendRequestAndCompare(INT_REQUEST_CTRUE, self.mockedHandler.elements['int'])
        self.sendRequestAndCompare(INT_REQUEST_CFALSE, self.mockedHandler.elements['filteredItem'])
        self.sendRequestAndCompare(INT_REQUEST_TRUE, self.mockedHandler.elements['int'])
        self.sendRequestAndCompare(INT_REQUEST_FALSE, self.mockedHandler.elements['filteredItem'])
        self.sendRequestAndCompare(INT_REQUEST_PARAM_TRUE, self.mockedHandler.elements['int'])
        self.sendRequestAndCompare(INT_REQUEST_PARAM_FALSE, self.mockedHandler.elements['filteredItem'])
        self.sendRequestAndCompare(INT_REQUEST_REMAP_TRUE, self.mockedHandler.elements['int'])

        # node with children
        response = self.dsRestAPI.parseRequest(NODE_WITH_CHILDREN, ADMIN)
        self.assertEqual(response['contentList'][0]['node']['val'], self.mockedHandler.elements['int']['node']['val'])
        self.assertEqual(response['contentList'][0]['node']['childVals'][0], self.mockedHandler.elements['string'])

        # list with children
        response = self.dsRestAPI.parseRequest(LIST_WITH_CHILDREN, ADMIN)
        self.assertEqual(len(response['contentList'][0]['list']), 3)
        for index, element in enumerate(response['contentList'][0]['list']):
            self.assertEqual(element['node']['val'], str(index))
            self.assertEqual(len(element['node']['childVals']), 1)
            self.assertEqual(element['node']['childVals'][0]['node']['val'], 'string_' + str(index))

        # list with selection
        response = self.dsRestAPI.parseRequest(LIST_WITH_SELECTION, ADMIN)
        self.assertEqual(len(response['contentList'][0]['list']), 3)
        self.assertNotIn('childVals', response['contentList'][0]['list'][0]['node'])
        self.assertEqual('string_1',  response['contentList'][0]['list'][1]['node']['childVals'][0]['node']['val'])
        self.assertNotIn('childVals', response['contentList'][0]['list'][2]['node'])

        # postfilter
        response = self.dsRestAPI.parseRequest(LIST_REQUEST_FILTERED, ADMIN)
        self.assertEqual(len(response['contentList'][0]['list']), 2)
        for index, element in enumerate(response['contentList'][0]['list']):
            self.assertEqual(element['node']['val'], str(index))
            self.assertEqual(len(element['node']['childVals']), 1)
            self.assertEqual(element['node']['childVals'][0]['node']['val'], 'string_' + str(index))

        self.sendRequestAndCompare(NODE_REQUEST_PARENT_TRUE, self.mockedHandler.elements['true'])
        response = self.dsRestAPI.parseRequest(NODE_REQUEST_PARENT_FALSE, ADMIN)
        self.assertEqual(response['contentList'][0]['node']['val'], '')
        self.assertEqual(response['contentList'][0]['node']['tp'], 0)

        # rangefilter
        response = self.dsRestAPI.parseRequest(LIST_REQUEST_WITH_RANGEFILTER, ADMIN)
        self.assertEqual(len(response['contentList'][0]['list']), 1)
        self.assertEqual(response['contentList'][0]['list'][0]['node']['val'], '1')

    def test_setData(self):
        '''
        Author: EDNIGBO Daniel Gobor
        Testcase: test_setData
        Tested component: ServiceFramework
        Feature: ServiceFramework - DsRestAPI
        Requirement: setData request handling
        Action_To_Be_taken:
            process requests and check responses
        Expected_Result: pass
        '''

        # user credentials
        self.dsRestAPI.parseRequest(INT_REQUEST_SET, WRONG_USER)
        response = self.dsRestAPI.parseRequest(INT_REQUEST, ADMIN)
        self.assertEqual(response['contentList'][0]['node']['val'], "1")

        # simple set
        self.dsRestAPI.parseRequest(INT_REQUEST_SET, ADMIN)
        response = self.dsRestAPI.parseRequest(INT_REQUEST, ADMIN)
        self.assertEqual(response['contentList'][0]['node']['val'], "-1")

        # set with params
        self.dsRestAPI.parseRequest(LIST_ELEMENT_SET, ADMIN)
        response = self.dsRestAPI.parseRequest(LIST_WITH_CHILDREN, ADMIN)
        self.assertEqual(response['contentList'][0]['list'][0]['node']['val'], "0")
        self.assertEqual(response['contentList'][0]['list'][1]['node']['val'], "1")
        self.assertEqual(response['contentList'][0]['list'][2]['node']['val'], "2")
        self.assertEqual(response['contentList'][0]['list'][0]['node']['childVals'][0]['node']['val'], "other_string")
        self.assertEqual(response['contentList'][0]['list'][1]['node']['childVals'][0]['node']['val'], "string_1")
        self.assertEqual(response['contentList'][0]['list'][2]['node']['childVals'][0]['node']['val'], "string_2")

        # set list element
        self.dsRestAPI.parseRequest(LIST_SET, ADMIN)
        response = self.dsRestAPI.parseRequest(LIST_REQUEST, ADMIN)
        self.assertEqual(response['contentList'][0]['list'][0]['node']['val'], "0")
        self.assertEqual(response['contentList'][0]['list'][1]['node']['val'], "-1")
        self.assertEqual(response['contentList'][0]['list'][2]['node']['val'], "2")

    def sendRequestAndCompare(self, request, expected, userCredentials = ADMIN):
        response = self.dsRestAPI.parseRequest(request, userCredentials)
        self.assertEqual(response['contentList'][0], expected)
